#!/bin/bash

# Script for making a movie from a setting file

path=$1
# fname="${path#*/}"
fname="$(basename $path)"
fname_sans_ext="${fname%.*}"
tmp_dir=".tmp_$fname_sans_ext"

mkdir -p "$tmp_dir"

interval=24
fps=10
colors=0
bitrate=200
if [ "$#" -ge "2" ]; then
    interval=$2
fi
if [ "$#" -ge "3" ]; then
    fps=$3
fi
if [ "$#" -ge "4" ]; then
    colors=$4
fi
if [ "$#" -ge "5" ]; then
    bitrate=$5
fi

# Creating snapshots as png files in a temporary directory
python -m src.create_snapshots.py "$path" "$tmp_dir" "$interval" "$colors"

# Creating movie from the snapshots
#ffmpeg -r 4 -pattern_type glob -i "$tmp_dir/*.png" -c:v libx264 -pix_fmt yuv420p "$fname_sans_ext.mp4"
# ffmpeg -pattern_type glob -i "$tmp_dir/*.png" -c:v libx264 -vf "fps=5,format=yuv420p" "$fname_sans_ext.mp4"
# ffmpeg -r 4 -pattern_type glob -i "$tmp_dir/*.png" -vcodec libx264 -crf 25 -pix_fmt yuv420p "$fname_sans_ext.mp4"
# mencoder "mf://$tmp_dir/*.png" -fps 4 -ovc lavc -lavcopts autoaspect=1 -of avi -o "$fname_sans_ext.avi"
# mencoder "mf://$tmp_dir/*.png" -mf w=800:h=600:fps=5:type=png -ovc lavc -lavcopts vcodec=mpeg4:mbd=2:trell -oac copy -o "$fname_sans_ext.avi"
# mencoder "mf://$tmp_dir/*.png" -mf fps=$fps:type=png -ovc lavc -lavcopts vcodec=mpeg4:mbd=2:vbitrate=$bitrate:trell -oac copy -o "$fname_sans_ext.mpeg4" # mpeg4 codec
# mencoder "mf://$tmp_dir/*.png" -mf fps=$fps:type=png -ovc lavc -lavcopts vcodec=mjpeg:mbd=2:vbitrate=$bitrate:trell -oac copy -o "$fname_sans_ext.avi" # mjpeg codec for Mac
# mencoder "mf://$tmp_dir/*.png" -mf fps=$fps:type=png -ovc x264 -x264encopts bitrate=$bitrate:pass=1:nr=2000 -oac copy -o "$fname_sans_ext.avi" # x264 codec
mencoder "mf://$tmp_dir/*.png" -mf fps=$fps:type=png -ovc xvid -xvidencopts bitrate=$bitrate -oac copy -o "$fname_sans_ext.mp4" # xvid codec
# ffmpeg -framerate $fps -pattern_type glob -i "$tmp_dir/*.jpg" -c:v libx264 -vf "pad=ceil(iw/2)*2:ceil(ih/2)*2" -crf 25 "${fname_sans_ext}_${variable}.mp4"

rm -rf "$tmp_dir"
