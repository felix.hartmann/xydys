# XyDyS (Xylogenesis Dynamics Simulator)

XyDyS is an open source software for modeling and simulating the dynamics of wood formation in conifers. Through a user-friendly interface, models can be interactively developed from basic building blocks, and then tuned, explored, and analyzed.

![Screenshot](images/screenshot.jpg "screenshot of XyDyS graphical interface")

All XyDyS models share a common set of features:

* A single radial file of cells is considered, from cambial initials to mature xylem cells.
* Cell identities and growth rates are determined by biochemical signals (hormones and/or peptides).
* Cell division is based on a geometrical criterion.
* Only cell division and expansion are modeled, not cell wall thickening and lignification.

A variety of possibilities are offered when defining the biochemical signals:

* A signal can determine cell identities (division, enlargement) and/or expansion rates.
* A signal can enter the radial cell file from either the phloem or the mature xylem.
* A signal can diffuse or be actively and polarly transported.
* Crosstalks are possible: A signal can influence how another signal is transported.

Outputs:

* The full spatiotemporal dynamics of the growing cell file, as time lapse.
* Growth curves.
* Cell diameters.
* Number of cells in each developmental zone, all along the growing season.

The following scientific publications are based on results obtained with XyDyS:

* Hartmann F.P., Rathgeber C.B.K., Badel É., Fournier M. & Moulia, B. (2021) Modelling the spatial crosstalk between two biochemical signals explains wood formation dynamics and tree-ring structure. *Journal of Experimental Botany*. https://doi.org/10.1093/jxb/eraa558

* Hartmann F.P., Rathgeber C.B.K., Fournier M. & Moulia, B. (2017) Modelling wood formation and structure: power and limits of a morphogenetic gradient in controlling xylem cell proliferation and growth. *Annals of Forest Science*. https://doi.org/10.1007/s13595-016-0613-y


# Installing XyDyS

First, [download the source code of XyDyS](https://forgemia.inra.fr/felix.hartmann/xydys/-/archive/master/xydys-master.zip) and extract the zip archive.

XyDyS is written in Python 3 and depends on several Python libraries. This means you need to have Python and these libraries installed on your machine to use XyDyS. Once you have Python installed, open a terminal, move to the XyDyS directory (the one you just extracted from the zip archive), and run the following command:

	pip install -r requirements.txt

This should automatically install all the required libraries.

Another, maybe simpler option, especially convenient for Mac and Windows users, is to [install the Anaconda distribution](https://www.anaconda.com/download/), which provides out-of-the-box Python and all the required libraries.


# Running XyDyS

## Windows

Double-click on the `XyDyS.bat` file in the XyDyS folder you downloaded.

## On Linux or Mac OS X

Open a terminal (possibly the Anaconda prompt), move to the XyDyS directory, and run the command:

	python ./xydys.py
