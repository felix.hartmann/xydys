#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
XyDyS (Xylogenesis Dynamics Simulator) is a software for modeling and simulating the
dynamics of wood formation in conifers. Through a user-friendly interface, models can be
interactively developed from basic building blocks, and then tuned, explored, and
analyzed.

Created in 2013.

@author: Félix Hartmann (felix.hartmann@inrae.fr)
"""

import matplotlib as mpl
mpl.use("Qt5Agg")

from src.controlpanel import ControlPanel

if __name__ == "__main__":
    import sys
    if len(sys.argv) > 1 and sys.argv[1] == "cli":
        # Command-line interface mode
        cp = ControlPanel()
    else: #  GUI mode
        if len(sys.argv) > 1 and sys.argv[1].endswith(".json"):
            # a settings file has been given
            ControlPanel(settings_file=sys.argv[1]).configure_traits()
        else:
            ControlPanel().configure_traits()
