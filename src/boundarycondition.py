#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Defining and computing boundary conditions.
"""

import numpy as np

from traits.api import HasTraits, Instance, Int, Enum, Button
from traitsui.api import View, Item, Group

from .evolution_function import (FunctionHandler, EvolutionFunction, Constant,
                                 Exponential, LogNormal, UserDefined)
from .plotwindow_qt import FunctionOfTimePlot


class BoundaryCondition(HasTraits):
    """
    Boundary condition for a signal diffusing in a radial cell file.

    A BoundaryCondition object is intended to be associated with a
    Signal object and contains all the information necessary to define
    a boundary condition during a simulation run.

    A boundary condition can be imposed on two possible boundaries:
    - the phloem side
    - the xylem side

    A boundary condition can be of two types:
    - imposed concentration
    - imposed flux
    """
    # boundary condition end point
    boundary = Enum(
        "phloem",
        "xylem",
        label="Boundary",
        desc="boundary on which the condition applies"
        )

    def get_node(self):
        if self.boundary == "phloem":
            return 0
        elif self.boundary == "xylem":
            return -1
        else:
            return None

    # boundary condition type
    kind = Enum(
        "concentration",
        "flux",
        label="Type",
        desc="type of boundary condition"
        )

    time_range = Int(0, label="Time range (hours)")

    # type of evolution function for the boundary condition
    evolution = Enum(
        "constant",
        "exponential decay",
        "log-normal",
        "user-defined",
        label="Evolution",
        desc="type of evolution function for the boundary condition"
        )

    _function_dict = {
        "constant": Constant,
        "log-normal": LogNormal,
        "exponential decay": Exponential,
        "user-defined": UserDefined
        }

    function = Instance(EvolutionFunction)

    def _function_default(self):
        return self._function_dict[self.evolution]()

    # This dictionary is intended to the FunctionHandler instance assigned to
    # the traits_view's handler attribute.
    _dict_for_functionhandler = {
        "evolution": "evolution",
        "function": "function"
        }

    # boundary condition values
    values = None

    def compute_values(self, start, stop, num):
        """
        Compute the values taken by the boundary condition during the
        simulation.

        Parameters
        ----------
        start, stop and num: ints
            Parameters for creating time points with Numpy's linspace function.
        """
        X = np.linspace(start, stop, num)
        return self.function.f(X)

    def set_values(self, start, stop, num):
        """
        Set the values taken by the boundary condition during the simulation.

        Parameters
        ----------
        start, stop and num: ints
            Parameters for creating time points with Numpy's linspace function.
        """
        self.values = self.compute_values(start, stop, num)

    function_of_time_plot = Instance(FunctionOfTimePlot)

    plot_function = Button("Plot function")

    def _plot_function_fired(self):
        """Callback of the "plot function" button."""
        if self.function_of_time_plot is None:
            self.function_of_time_plot = FunctionOfTimePlot(
                parent=self, function=self.function)
            self.function_of_time_plot.edit_traits()
        else:
            self.function_of_time_plot.update_plot()

    general_group = Group(
        Item('kind'),
        Item('evolution'),
        springy=True
        )

    function_group = Group(
        Group(
            Item('function',
                 style='custom',
                 springy=True,
                 show_label=False
                 ),
            show_labels=False
            ),
        label="Definition of the evolution function",
        springy=True,
        show_border=True
        )

    traits_view = View(
        Group(
            general_group,
            function_group,
            Item('plot_function', show_label=False),
            springy=True
            ),
        handler=FunctionHandler(),
        resizable=True
        )
