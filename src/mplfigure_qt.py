#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""TraitsUI Matplotlib editor using Qt backend.
Publicaly shared by Ryan Olf, April 2013.
Pointed out to me by Pierre Haessig, March 2014.

Source:
http://enthought-dev.117412.n3.nabble.com/traitsui-matplotlib-editor-with-toolbar-using-qt-backend-td4026437.html

April 2019: a few changes for accomodating PyQt5, based on
https://pythonspot.com/pyqt5-matplotlib/

July 2023: the tricks to accomodate PyQt5 are no longer needed.
"""

from PyQt5.QtWidgets import QWidget, QVBoxLayout
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT
from matplotlib.figure import Figure

from traits.api import Instance
from traitsui.qt.editor import Editor
from traitsui.basic_editor_factory import BasicEditorFactory


class _MPLFigureEditor(Editor):

    scrollable = True

    def init(self, parent):
        self.control = self._create_canvas(parent)
        self.set_tooltip()

    def update_editor(self):
        pass

    def _create_canvas(self, parent):
        """Create the MPL canvas."""
        frame = QWidget()
        mpl_canvas = FigureCanvas(self.value)
        mpl_canvas.setParent(frame)
        mpl_toolbar = NavigationToolbar2QT(mpl_canvas, frame)

        vbox = QVBoxLayout()
        vbox.addWidget(mpl_canvas)
        vbox.addWidget(mpl_toolbar)
        frame.setLayout(vbox)

        return frame


class MPLFigureEditor(BasicEditorFactory):
    klass = _MPLFigureEditor
