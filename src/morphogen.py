#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Morphogen class.
"""

from .boundarycondition import BoundaryCondition
from .transport import (TransportHandler, Transport, UniformDiffusion)

from traits.api import HasTraits, Instance, Int, Str, Enum
from traitsui.api import View, Item, Group, VGroup, VFold


class Morphogen(HasTraits):
    """
    A morphogen moving in a radial cell file.

    A Morphogen object is intended to be an input to a simulation of
    the growth of a radial cell file. It contains informations on how a signal
    is transported in that file (diffusion, active transport...)
    plus phloem and xylem boundary conditions (as BoundaryCondition objects).
    """
    number = Int
    name = Str

    def _number_changed(self):
        self.name = "Signal {}".format(self.number)

    def __init__(self, number=-1):
        self.number = number

    # Transport mechanism
    transport_mechanism = Enum(
        "uniform diffusion",
        "Smith model (2006)",
        "simple unidirectional active transport",
        "imposed cell concentrations",
        label="Transport mechanism",
        desc="way the morphogen is transported along the file.")

    transport = Instance(Transport)

    def _transport_default(self):
        return UniformDiffusion()

    # Boundary conditions
    phloem_BC = Instance(BoundaryCondition)
    xylem_BC = Instance(BoundaryCondition)

    def _phloem_BC_default(self):
        return BoundaryCondition(boundary="phloem")

    def _xylem_BC_default(self):
        return BoundaryCondition(boundary="xylem")

    def boundary_conditions(self):
        """
        Return the list of the boundary conditions of the morphogen, as
        BoundaryCondition objects.
        """
        return [self.phloem_BC, self.xylem_BC]

    def nb_concentrations(self):
        """Return the number of imposed concentrations on the signal."""
        nb_conc = 0
        # TODO
        # the boundary_conditions method should be called instead
        for bcond in (self.phloem_BC, self.xylem_BC):
            if bcond.kind == "concentration":
                nb_conc += 1
        return nb_conc

    def nb_flux(self, ignore_zero_flux=False):
        """Return the number of imposed flux on the signal.

        If ignore_zero_flux is True, constant zero flux are not counted.
        """
        nb_flux = 0
        # TODO
        # the boundary_conditions method should be called instead
        for bcond in (self.phloem_BC, self.xylem_BC):
            if bcond.kind == "flux":
                nb_flux += 1
        return nb_flux

    def set_values(self, start, stop, num, signals=None):
        """
        Set various values, time-variable or not, associated with the signal.
        Namely:
        - Values taken by the boundary conditions during the simulation.
        - Values taken by time-dependant parameters of the transport mechanism.
        - Information on how signal transport depends on other signals.

        Parameters
        ----------
        start, stop and num: ints
            Parameters for creating time points with Numpy's linspace function.
        signals: iterable of Morphogen instances, optional
            All signals involved in the simulation.
        """
        # boundary conditions values
        for bcond in self.boundary_conditions():
            bcond.set_values(start, stop, num)
        # values of the time-dependant parameters of the transport mechanism
        # and information on how signal transport depends on other signals
        self.transport.set_values(start, stop, num, signals=signals)

    def __repr__(self):
        return self.name

    def __getitem__(self, key):
        """Make the object behave like a tuple.

        This is an ingenious/atrocious hack so that a List trait of Morphogen
        instances can be passed as 'name' argument of a CheckListEditor.

        No longer used.

        Source:
        http://stackoverflow.com/questions/19841160/using-checklisteditor-with-something-other-than-liststr
        """
        if key == 0:
            return self
        elif key == 1:
            return self.__repr__()
        else:
            raise KeyError

    transport_group = Group(
        Group(
            Item('transport',
                 style='custom',
                 springy=True,
                 show_label=False
                 ),
            show_labels=False
            ),
        label="Transport parameters",
        springy=True,
        show_border=True
        )

    traits_view = View(
        VGroup(
            VFold(
                Group(
                    Item('transport_mechanism'),
                    transport_group,
                    label="Transport mechanism",
                    springy=True
                    ),
                Group(
                    Item('phloem_BC',
                         style='custom',
                         show_label=False,
                         springy=True),
                    label="Phloem boundary condition",
                    springy=True
                    ),
                Group(
                    Item('xylem_BC',
                         style='custom',
                         show_label=False,
                         springy=True),
                    label="Xylem boundary condition",
                    springy=True
                    ),
                springy=True
                ),
            ),
        handler=TransportHandler,
        resizable=True
        )
