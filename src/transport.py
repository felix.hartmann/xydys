#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Some classes defining various types of signal transport.
"""

import numpy as np

from traits.api import (HasTraits, Instance, Bool, Int, Float, Str, Enum, Code)
from traitsui.api import (View, Item, Group, Handler, CodeEditor)

from .evolution_function import (FunctionHandler, EvolutionFunction, Constant,
                                 Exponential, LogNormal, UserDefined)


class TransportHandler(Handler):
    """Handler class to perform restructuring action when conditions are met.

    The restructuring consists of replacing a Transport instance by an other
    one.
    """
    def object_transport_mechanism_changed(self, info):
        if ((info.object.transport_mechanism == "uniform diffusion") and
                (not isinstance(info.object.transport, UniformDiffusion))):
            info.object.transport = UniformDiffusion()
        elif ((info.object.transport_mechanism
                == "simple unidirectional active transport") and
                (not isinstance(info.object.transport,
                 SimpleUnidirectionalActiveTransport))):
            info.object.transport = SimpleUnidirectionalActiveTransport()
        elif ((info.object.transport_mechanism
                == "Smith model (2006)") and
                (not isinstance(info.object.transport, SmithModel2006))):
            info.object.transport = SmithModel2006()
        elif ((info.object.transport_mechanism
                == "imposed cell concentrations") and
                (not isinstance(info.object.transport,
                 ImposedCellConcentrations))):
            info.object.transport = ImposedCellConcentrations()


class Transport(HasTraits):
    """
    An empty class from which all particular transport type classes inherit.
    """
    def set_values(self, start, stop, num, signals=None):
        pass


class UniformDiffusion(Transport):
    """
    Morphogens diffuse throughout the file as if it was a continuous medium
    with a uniform coefficient of diffusion.

    Of course, this medium is growing nonuniformly.
    """
    time = 0  # necessary for time-varying parameters

    D = Float(0.1,
              label="Diffusion coefficient (µm.s¯²)",
              desc="diffusion coefficient of the signal, in µm.s¯², "
                   "uniform along the whole file.")

    # type of evolution function for the decay rate
    d_evolution = Enum(
        "constant",
        "exponential decay",
        "log-normal",
        "user-defined",
        label="Decay rate evolution",
        desc="type of evolution function for the decay rate of the "
             "signal (s¯¹), constant along the whole file.")

    _function_dict = {
        "constant": Constant,
        "log-normal": LogNormal,
        "exponential decay": Exponential,
        "user-defined": UserDefined}

    d_function = Instance(EvolutionFunction)

    def _d_function_default(self):
        return self._function_dict[self.d_evolution]()

    # This dictionary is intended to the FunctionHandler instance assigned to
    # the traits_view's handler attribute.
    _dict_for_functionhandler = {
        "evolution": "d_evolution",
        "function": "d_function"}

    # decay rate values
    d_values = None

    def compute_d_values(self, start, stop, num):
        """
        Compute the values taken by the decay rate during the simulation.
        """
        X = np.linspace(start, stop, num)
        return self.d_function.f(X)

    def set_d_values(self, start, stop, num):
        """
        Set the values taken by the decay rate during the simulation.
        """
        self.d_values = self.compute_d_values(start, stop, num)

    @property
    def d(self):
        return self.d_values[self.time]

    @d.setter
    def d(self):
        raise AttributeError("Sorry, the decay rate can not assigned that way."
                             "Use the d_function trait instead.")

    s = Float(0.,
              label="Production rate (m¯³.s¯¹)",
              desc="production rate of the signal, in m¯³.s¯¹, "
                   "constant along the whole file.")

    P = Float(0., label="Active tranport coefficient")

    steady_state = Bool(False,
                        label="Steady state approximation",
                        desc="if the steady state approximation can be made.")

    def set_values(self, start, stop, num, signals=None):
        """
        Set the values of the time-varying parameters.
        """
        self.set_d_values(start, stop, num)

    general_group = Group(Item('D'),
                          Item('d_evolution'),
                          springy=True)

    function_group = Group(
        Group(
            Item('d_function',
                 style='custom',
                 springy=True,
                 show_label=False
                 ),
            show_labels=False
            ),
        label="Definition of the evolution function",
        springy=True,
        show_border=True
        )

    traits_view = View(
        Group(
            general_group,
            function_group,
            # Item('plot_function', show_label = False),
            Item('steady_state'),
            springy=True
            ),
        handler=FunctionHandler(),
        resizable=True
        )

# end of the UniformDiffusion class


class SimpleUnidirectionalActiveTransport(Transport):
    """
    Transport mechanism described in Grieneisen et al., 2012,
    doi: 10.1186/1752-0509-6-37
    """
    time = 0  # necessary for time-varying parameters

    # type of evolution function for the polar transport coefficient p
    p_evolution = Enum(
        "constant",
        "exponential decay",
        "log-normal",
        "user-defined",
        label="Evolution of the polar transport",
        desc="type of evolution function for the polar transport coefficient"
             "of the signal, coefficient accounting for the action of polar"
             "efflux carriers, in µm.s¯¹.")

    _function_dict = {
        "constant": Constant,
        "log-normal": LogNormal,
        "exponential decay": Exponential,
        "user-defined": UserDefined}

    p_function = Instance(EvolutionFunction)

    def _p_function_default(self):
        return self._function_dict[self.p_evolution]()

    # This dictionary is intended to the FunctionHandler instance assigned to
    # the traits_view's handler attribute.
    _dict_for_functionhandler = {
        "evolution": "p_evolution",
        "function": "p_function"}

    # polar transport coefficient values
    p_values = None

    def compute_p_values(self, start, stop, num):
        """
        Compute the values taken by the polar transport coefficient during the
        simulation.

        - start, stop and num are the parameters for creating time points with
          Numpy's linspace function.
        """
        X = np.linspace(start, stop, num)
        return self.p_function.f(X)

    def set_p_values(self, start, stop, num):
        """
        Set the values taken by the polar transport coefficient during the
        simulation.
        """
        self.p_values = self.compute_p_values(start, stop, num)

    @property
    def p(self):
        return self.p_values[self.time]

    @p.setter
    def p(self):
        raise AttributeError("Sorry, the decay rate can not assigned that way."
                             "Use the p_function trait instead.")

    def p_max(self):
        """Return the maximal value of p."""
        return np.max(self.p_values)

    q = Float(1,
              label="Permeability rate (µm.s¯¹)",
              desc="permeability rate of the membranes, in µm.s¯¹.")

    d = Float(0.,
              label="Decay rate (s¯¹)",
              desc="decay rate of the signal, in s¯¹, "
                   "constant along the whole file.")

    s = Float(0.,
              label="Production rate (m¯³.s¯¹)",
              desc="production rate of the signal, in m¯³.s¯¹, "
                   "constant along the whole file.")

    carrier_orientation = Enum(
        "toward the cambium",
        "toward the xylem",
        label="Carrier orientation",
        desc="in which direction the carriers are oriented.")

    only_in_cambial_cells = Bool(
        label="Polar transport only in cambial cells",
        desc="whether polar transport is limited to cambial cells.")

    signals_controling_polar_transport = Str(
        "",
        label="Signals controling_polar_transport",
        desc="which signals, if any, control the value of the polar transport "
             "coefficient. Coma-separated list of signal names. Signals "
             "listed twice are raised to the second power, and so on.")

    polar_prefactor = Float(
        0,
        label="Polar transport prefactor",
        desc="prefactor k_p multiplying signal concentrations involve in the "
             "value of the polar transport coefficient.")

    polar_control_indices = []

    def set_values(self, start, stop, num, signals=None):
        """
        Set values associated with the signal.

        Parameters
        ----------
        start, stop and num: ints
            Parameters for creating time points with Numpy's linspace function.
        signals: iterable of Morphogen instances, optional
            All signals involved in the simulation.
        """
        # First, set the values of constitutive polar transport.
        self.set_p_values(start, stop, num)
        # Second, look for the indices of signal controling the coefficient of
        # polar transport.
        if signals is not None:
            names = [sig.strip() for sig in
                     self.signals_controling_polar_transport.split(",") if sig]
            self.polar_control_indices = []
            for name in names:
                try:
                    signal_names = [signal.name for signal in signals]
                    self.polar_control_indices.append(signal_names.index(name))
                except ValueError:
                    print("Warning: {0} is not a valid signal name for polar "
                          "transport control".format(name))

    general_group = Group(Item('d'),
                          Item('q'),
                          Item('p_evolution'),
                          springy=True)

    function_group = Group(
        Group(
            Item('p_function',
                 style='custom',
                 springy=True,
                 show_label=False
                 ),
            show_labels=False
            ),
        label="Definition of the evolution function",
        springy=True,
        show_border=True
        )

    traits_view = View(
        Group(
            general_group,
            function_group,
            # Item('plot_function', show_label = False),
            # Item('s'),
            Item('carrier_orientation'),
            Item('only_in_cambial_cells'),
            Item('signals_controling_polar_transport'),
            Item('polar_prefactor'),
            springy=True
            ),
        handler=FunctionHandler(),
        resizable=True
        )

# end of the SimpleUnidirectionalActiveTransport class


class SmithModel2006(Transport):
    """
    Auxin patterning model described in Smith et al., 2006,
    doi: 10.1073/pnas.0510457103
    """
    rho_IAA = Float(3,
                    label="Auxin production (s¯¹)",
                    desc="coefficient of auxin production, in s¯¹.")

    mu_IAA = Float(0.35,
                   label="Auxin degradation (s¯¹)",
                   desc="coefficient of auxin production, in s¯¹.")

    rho_PIN = Float(0.1,
                    label="PIN production (s¯¹)",
                    desc="coefficient of PIN production, in s¯¹.")

    mu_PIN = Float(0.3,
                   label="PIN degradation (s¯¹)",
                   desc="coefficient of PIN production, in s¯¹.")

    D = Float(0.5,
              label="Membrane permeability",
              desc="permeability of the membrane to auxin.")

    T = Float(22,
              label="Strength of active transport",
              desc="strength of auxin active transport.")

    nb_cells_PIN = Int(
        8,
        label="Number of PIN-producing cells",
        desc="number of cells, counting from the cambiuum side, which produce "
             "PIN proteins.")

    nb_cells_IAA = Int(
        14,
        label="Number of auxin-producing cells",
        desc="number of cells, counting from the cambiuum side, which produce "
             "auxin.")

    traits_view = View(
        Item('rho_IAA'),
        Item('mu_IAA'),
        Item('rho_PIN'),
        Item('mu_PIN'),
        Item('D'),
        Item('T'),
        Item('nb_cells_PIN'),
        Item('nb_cells_IAA'),
        resizable=True)

    def evolution_conc(self, u, PIN, L_cell, S, zones, dt, nb_steps):
        """Morphogen gradient evolution, with imposed boundary concentrations.

        This method changes the concentrations of the morphogen using the
        explicit Euler method, with a given number of time steps.

        It is assumed that boundary concentrations are imposed, so the boundary
        concentrations are not modified.

        Parameters
        ----------
        u : 1-D ndarray of floats
            Initial morphogen concentrations.
        PIN : 1-D ndarray of floats
            Initial PIN protein concentrations.
        L_cell : 1-D ndarray of floats
            Lengths of cells, in the real space.
        S : 1-D ndarray of floats
            Cell strain rates.
        zones : iterable
        dt : float
            Unit time step.
        nb_steps : int
            Number of time steps.

        Outputs
        -------
        u : 1-D ndarray of floats
            Morphogen concentrations after changes.
        PIN : 1-D ndarray of floats
            PIN protein concentrations after changes.

        """
        nb_cells = u.shape[0]
        if self.nb_cells_IAA >= nb_cells:
            nb_cells_IAA = nb_cells - 1
        else:
            nb_cells_IAA = self.nb_cells_IAA
        if self.nb_cells_PIN >= nb_cells:
            nb_cells_PIN = nb_cells - 1
        else:
            nb_cells_PIN = self.nb_cells_PIN
        PIN[nb_cells_PIN:] = 0
        D_eff = self.D  # / L_celll[1:-1]
        T_eff = self.T  # / L_celll[1:-1]
        PINwall = np.zeros((nb_cells, 2))
        if None not in zones:
            if 0 in zones[0]:
                PINwall[0, 1] = 0.5
            if nb_cells - 1 in zones[0]:
                PINwall[nb_cells-1, 0] = 0.5
            zone_1 = zones[0]
            zone_1 = zone_1[zone_1 > 0]
            zone_1 = zone_1[zone_1 < nb_cells - 1]
            zone_2 = zones[0]
            zone_2 = zone_2[zone_2 > 0]
            zone_2 = zone_2[zone_2 < nb_cells - 1]
            zone_3 = zones[0]
            zone_3 = zone_3[zone_3 > 0]
            zone_3 = zone_3[zone_3 < nb_cells - 1]
        else:
            zone_1, zone_2, zone_3 = range(1, -1), range(1, -1), range(1, -1)
        for i in range(nb_steps):
            # PIN distribution between cell membranes
            for c in zone_1:  # range(1, nb_cells_PIN - 1): #nb_cells - 1):
                PINwall[c, 0], PINwall[c, 1] = np.exp(u[c-1]), np.exp(u[c+1])
                summa = PINwall[c].sum()
                PINwall[c] *= PIN[c] / summa
            # morphogen concentration change with in a time step
            u[1:-1] += dt * (
                - self.mu_IAA * u[1:-1]
                + D_eff * (u[:-2] + u[2:] - 2*u[1:-1])
                + T_eff * (
                    PINwall[:-2, 1] * u[:-2]**2 / (1 + u[1:-1])
                    + PINwall[2:, 0] * u[2:]**2 / (1 + u[1:-1])
                    - PINwall[1:-1, 0] * u[1:-1]**2 / (1 + u[:-2])
                    - PINwall[1:-1, 1] * u[1:-1]**2 / (1 + u[2:])
                    )
                )
            u[zone_2] += dt * self.rho_IAA / (1 + u[zone_2])
            # PIN concentration change with in a time step
            PIN[zone_1] += dt * (
                self.rho_PIN * u[zone_1] / (1 + PIN[zone_1])
                - self.mu_PIN * PIN[zone_1])
        return u, PIN

# end of the SmithModel2006 class


class ImposedCellConcentrations(Transport):
    """
    Each cell gets a morphogen concentration based on its position along the
    file.
    """
    function_definition = Code(
        "def f(t, n, phloem_BC, xylem_BC):\n"
        "    # Write whatever you want here,\n"
        "    # but please don't crash nor hack the system.\n"
        "    # t: time (days)\n"
        "    # n: cell index, (the cell closest to phloem has index 0)\n"
        "    # phloem_BC: concentration of the cell closest to phloem\n"
        "    #            (if phloem boundary concentration were given)\n"
        "    # xylem_BC: concentration of the cell closest to xylem\n"
        "    #           (if xylem boundary concentrations were given)\n"
        "    return ",
        show_label=False)

    def f(self, t, N, phloem_BC, xylem_BC):
        """The actual performative function itself."""
        exec(self.function_definition)  # define a function f
        Y = np.zeros(N)
        for n in range(N):
            Y[n] = f(t, n, phloem_BC, xylem_BC)
        return Y

    traits_view = View(
        Item('function_definition',
             editor=CodeEditor(auto_set=False),
             show_label=False),
        resizable=True,
        scrollable=True)

    def cell_concentrations(self, t, N, phloem_BC, xylem_BC):
        """
        Return the array of cell concentrations as given by the user-defined
        functions.
        """
        return self.f(t, N, phloem_BC, xylem_BC)
