#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
PlottingControl class.
Used for opening and controlling secondary windows displaying the simulation
outputs. Embedded in a ControlPanel instance.
"""

from .plotwindow_qt import (SnapshotPlot, GrowthPlot, DynamicsPlot,
                            CellProcessPlot)

from traits.api import (HasTraits, Instance, Bool, Button)
from traitsui.api import (View, Item, HGroup)


class PlottingControl(HasTraits):
    """Part of the GUI that launches the plotting of outputs.

    A PlottingControl instance is intended to be an element of the main window
    (which is a ControlPanel instance). It is only created after a simulation
    has been performed and allows to open secondary windows displaying the
    simulation outputs.

    """
    # cell order and reference point
    fixed_cambium_right = True

    # checkboxes for selecting what to display on the spatial representation of
    # the cell file
    division_threshold = Bool(True, label="division threshold")
    enlargement_threshold = Bool(True, label="end of enlargement threshold")
    strain_rates = Bool(False, label="strain rates")
    colored_zones = Bool(True, label="colored zones")

    # various types of plot windows
    cell_file_plot = Instance(SnapshotPlot)
    cell_diameters_plot = Instance(SnapshotPlot)
    total_growth_plot = Instance(GrowthPlot)
    total_growth_speed_plot = Instance(GrowthPlot)
    cell_accumulation_plot = Instance(DynamicsPlot)
    number_of_cells_in_each_phase_plot = Instance(DynamicsPlot)
    cell_entry_rates_plot = Instance(DynamicsPlot)
    cell_residence_times_plot = Instance(CellProcessPlot)
    cell_mean_rates_plot = Instance(CellProcessPlot)

    # buttons for launching plots
    draw_cell_file = Button("Draw cell file")
    plot_cell_diameters = Button("Plot cell diameters")
    plot_total_growth = Button("Plot growth")
    plot_total_growth_speed = Button("Plot growth speed")
    plot_cell_accumulation = Button("Plot cell accumulation")
    plot_number_of_cells_in_each_phase = Button("Plot number of cells in each "
                                                "phase")
    plot_cell_entry_rates = Button("Plot cell entry rate in each phase")
    plot_cell_residence_times = Button("Plot cell residence times")
    plot_mean_rates = Button("Plot mean rates")

    traits_view = View(
        Item('draw_cell_file', show_label=False),
        HGroup(
            Item('division_threshold'),
            Item('enlargement_threshold'),
            Item('strain_rates'),
            Item('colored_zones')
            ),
        Item('plot_cell_diameters', show_label=False),
        Item('plot_total_growth', show_label=False),
        Item('plot_total_growth_speed', show_label=False),
        Item('plot_cell_accumulation', show_label=False),
        Item('plot_number_of_cells_in_each_phase', show_label=False),
        Item('plot_cell_entry_rates', show_label=False),
        Item('plot_cell_residence_times', show_label=False),
        Item('plot_mean_rates', show_label=False),
        resizable=True
        )

    def __init__(self, cell_file, start, stop, activation_time=None):
        self.cell_file = cell_file
        self.start = start
        self.stop = stop
        self.activation_time = activation_time

    def _draw_cell_file_fired(self):
        """Callback of the "draw cell file" button."""
        self.cell_file_plot = SnapshotPlot(
            self.cell_file, "cell file",
            self.start, self.stop,
            fixed_cambium_right=self.fixed_cambium_right
            )
        self.cell_file_plot.edit_traits()
        self.prepare_snapshot_plot(self.cell_file_plot)

    def _plot_cell_diameters_fired(self):
        """Callback of the "plot cell diameters" button."""
        self.cell_diameters_plot = SnapshotPlot(
            self.cell_file, "diameters",
            self.start, self.stop,
            fixed_cambium_right=self.fixed_cambium_right
            )
        self.cell_diameters_plot.edit_traits()
        self.prepare_snapshot_plot(self.cell_diameters_plot)

    def prepare_snapshot_plot(self, snapshot_plot):
        snapshot_plot.show_strain_rates = self.strain_rates
        snapshot_plot.colored_zones = self.colored_zones
        self.on_trait_change(self.update_strain_rates, 'strain_rates')
        self.on_trait_change(self.update_colored_zones, 'colored_zones')
        self.on_trait_change(self.update_threshold_list,
                             'division_threshold, enlargement_threshold')
        self.update_threshold_list()

    def _plot_total_growth_fired(self):
        """Callback of the "plot total growth" button."""
        self.total_growth_plot = GrowthPlot(self.cell_file, "length")
        self.total_growth_plot.edit_traits()

    def _plot_total_growth_speed_fired(self):
        """Callback of the "plot total growth" button."""
        self.total_growth_speed_plot = GrowthPlot(self.cell_file, "speed")
        self.total_growth_speed_plot.edit_traits()

    def _plot_cell_accumulation_fired(self):
        """Callback of the "plot cell accumulation" button."""
        self.cell_accumulation_plot = DynamicsPlot(
            self.cell_file,
            "Accumulation of cells produced",
            self.start, self.stop
            )
        self.cell_accumulation_plot.edit_traits()

    def _plot_number_of_cells_in_each_phase_fired(self):
        """Callback of the "plot number of cells in each phase" button."""
        print("Plot numbers launched")
        self.number_of_cells_in_each_phase_plot = DynamicsPlot(
            self.cell_file,
            "Number of cells in each phase",
            self.start, self.stop
            )
        print("DynamicsPlot inited")
        self.number_of_cells_in_each_phase_plot.edit_traits()
        print("Edit traits")

    def _plot_cell_entry_rates_fired(self):
        """Callback of the "plot cell entry rates" button."""
        self.cell_entry_rates_plot = DynamicsPlot(
            self.cell_file,
            "Cell entry rate in each phase",
            self.start, self.stop
            )
        self.cell_entry_rates_plot.edit_traits()

    def _plot_cell_residence_times_fired(self):
        """Callback of the "plot cell residence times" button."""
        self.cell_residence_times_plot = CellProcessPlot(
            self.cell_file,
            "Cell residence time in each phase",
            fixed_cambium_right=self.fixed_cambium_right
            )
        self.cell_residence_times_plot.edit_traits()

    def _plot_mean_rates_fired(self):
        """Callback of the "plot mean rates" button."""
        self.cell_mean_rates_plot = CellProcessPlot(
            self.cell_file,
            "Mean rate in each phase",
            fixed_cambium_right=self.fixed_cambium_right
            )
        self.cell_mean_rates_plot.edit_traits()

    def _create_snapshots_fired(self):
        """Callback of the "create snapshots" button."""
        time = int(24 * self.snapshot_time)
        for t in range(0, time, 3):
            self.cell_file.draw(
                t,
                xlim=(0, 320),
                date=True,
                save=True,
                base_name="growth3/growth3"
                )

    def update_strain_rates(self):
        for plot in [self.cell_file_plot, self.cell_diameters_plot]:
            if plot is not None:
                plot.show_strain_rates = self.strain_rates
                plot.update_plot()

    def update_colored_zones(self):
        for plot in [self.cell_file_plot, self.cell_diameters_plot]:
            if plot is not None:
                plot.colored_zones = self.colored_zones
                plot.update_plot()

    def update_threshold_list(self):
        threshold_list = []
        if self.division_threshold:
            threshold_list.append(self.cell_file.division_threshold)
        if self.enlargement_threshold:
            threshold_list.append(self.cell_file.enlargement_threshold)
        for plot in [self.cell_file_plot, self.cell_diameters_plot]:
            if plot is not None:
                plot.threshold_list = threshold_list
                plot.update_plot()
