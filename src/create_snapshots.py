#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import bisect
import numpy as np

def number_of_digits(a):
    if a == 0:
        a = 1
    return int(np.log10(a)) + 1

from traits.etsconfig.api import ETSConfig
ETSConfig.toolkit = 'qt'

import matplotlib as mpl

from .controlpanel import ControlPanel, time_units
from .plotutils import draw_cell_file

settings_file, target_dir = sys.argv[1:3]
base_name = target_dir + "/"

try:
    interval = int(sys.argv[3])
except IndexError:
    interval = 24

try:
    colors = bool(int(sys.argv[4]))
except IndexError:
    colors = False

cp = ControlPanel()
cp.set_from_file(settings_file)
cp.growth()

total_time = (cp.stop_time - cp.start_time) * time_units["day"]
total_hours = int(total_time / time_units["hour"])
total_snapshot_time = int(total_time / cp.snapshot_time_step)
nb_digits_max = number_of_digits(total_snapshot_time)

cf = cp.cell_file
total_length = cf.length()
length_list = [total_length/3, 2*total_length/3]
xlim_list = [(0, 1.01 * length) for length in length_list]
xlim_list += [(0, 1.01 * total_length)]
print(length_list)
print(xlim_list)

ylim = (0, 2.5)   # ylim = (0, 2.5)

print(total_hours, interval)
for hour in range(0, total_hours, interval):
    time = int(hour * time_units["hour"] / cp.snapshot_time_step)
    length = cf.length(time)
    framing_index = bisect.bisect(length_list, length)
    xlim = xlim_list[framing_index]
    nb_digits = number_of_digits(time)
    padding_0 = nb_digits_max - nb_digits
    draw_cell_file(cf,
                   time,
                   colored_zones=colors,
                   show_transport_polarity=None,
                   xlim=xlim,
                   ylim=ylim,
                   figsize=(8, 4),
                   fontsize=14,
                   save=True,
                   base_name=base_name+padding_0*"0")

#     cf.draw(time=hour,
#             signal=False,
#             cell_concentrations=range(cf.nb_signals),
#             xlim=xlim,
#             date=False,
#             save=True,
