#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Tools for defining the evolution function of a quantity or a boundary
condition.

EvolutionFunction is a base class from which each function class inherits.

FunctionHandler is a Traits Handler class for switching the evolution function
of an instance.
"""

import numpy as np

from traits.api import HasTraits, Str, Int, Float, Code
from traitsui.api import View, Item, Group, Handler, CodeEditor

from .plotutils import savitzky_golay

def log_normal(X, mu, sigma, A,
               absolute_threshold=None, relative_threshold=None):
    """Calculate the log-normal of all elements in the input array.

        Y = A/(X*sigma*sqrt(2*pi)) * exp(-(log(X) - mu)**2/(2*sigma**2))

    The output array can be thresholded by either an absolute or a
    relative threshold.

    """
    Y = np.empty(X.shape)
    if X[0] == 0.:
        Y[0] = 0.
        Y[1:] = log_normal(X[1:], mu, sigma, A)
    else:
        preexp = A/(X * sigma * np.sqrt(2*np.pi))
        Y = preexp * np.exp(-(np.log(X) - mu)**2/(2*sigma**2))
    if absolute_threshold is not None:
        Y[:np.where(Y > absolute_threshold)[0][0]] = 0.
    if relative_threshold is not None:
        threshold = relative_threshold * Y.max()
        Y[:np.where(Y > threshold)[0][0]] = 0.
    return Y


class EvolutionFunction(HasTraits):
    """
    An empty class from which all particular evolution function classes are
    derived.
    """
    pass


class Constant(EvolutionFunction):
    """Constant function."""

    equation = Str("f(t) = f_0")
    constant_value = Float(2, label="f_0")

    def f(self, X):
        """The actual performative function itself."""
        return self.constant_value * np.ones(X.shape)

    traits_view = View(
        Item("equation", style='readonly'),
        'constant_value',
        resizable=True,
        scrollable=True
        )


class LogNormal(EvolutionFunction):
    """Log-normal function."""

    equation = Str("f(t) = A/(t * sigma * sqrt(2*pi)) * "
                   "exp( -(log(t) - mu)**2 / (2 * sigma**2) )")

    mu = Float(label="mu")
    sigma = Float(label="sigma")
    A = Float(label="A")
    absolute_threshold = Float
    relative_threshold = Float

    def f(self, X):
        """The actual performative function itself."""
        Y = np.empty(X.shape)
        if X[0] == 0.:
            Y[0] = 0.
            Y[1:] = self.f(X[1:])
        else:
            preexp = self.A / (X * self.sigma * np.sqrt(2 * np.pi))
            Y = preexp * np.exp(-(np.log(X) - self.mu)**2
                                 / (2 * self.sigma**2) )
        return Y

    traits_view = View(
        Item("equation", style='readonly'),
        Item('mu'),
        Item('sigma'),
        Item('A'),
        resizable=True,
        scrollable=True
        )


class Exponential(EvolutionFunction):
    """Exponential decay."""

    equation = Str("f(t) = f_0 * exp( -t/tau )")

    f_0 = Float(2, label="f_0")
    tau = Float(150, label="tau")

    def f(self, X):
        """The actual performative function itself."""
        return self.f_0 * np.exp(-X/self.tau)

    traits_view = View(
        Item("equation", style='readonly'),
        Item('f_0'),
        Item('tau'),
        Item(""),
        resizable=True,
        scrollable=True
        )


class UserDefined(EvolutionFunction):
    """User-defined function."""

    function_definition = Code(
        "def f(t):\n    # Write whatever you want here,\n"
        "    # but please don't crash nor hack the system.\n    return ",
        show_label=False
        )

    smoothing_window = Int(
        0, label="Smoothing window",
        desc="the width (in days) of the window for the moving average (if 0, do nothing)")

    def f(self, X):
        """The actual performative function itself."""
        namespace = {}
        exec(self.function_definition, namespace)  # define a function f
        Y = np.zeros(len(X))
        for i, x in enumerate(X):
            Y[i] = namespace['f'](x)
        smoothing_window = int(self.smoothing_window * (len(X) - 1) / (X[-1] - X[0]))
        return savitzky_golay(Y, smoothing_window)

    traits_view = View(
        Group(
            Item('function_definition',
                 editor=CodeEditor(auto_set=False),
                 show_label=False
                 )
            ),
        Item('smoothing_window'),
        resizable=True,
        scrollable=True
        )


class FunctionHandler(Handler):
    """Handler class to perform restructuring action when conditions are met.

    The restructuring consists of replacing an EvolutionFunction instance by
    an other one.
    """

    def replace_evolution_function(self, object,
                                   evolution, function, function_name):
        if ((evolution == "constant") and
                (not isinstance(function, Constant))):
            setattr(object, function_name, Constant())
        elif ((evolution == "log-normal") and
                (not isinstance(function, LogNormal))):
            setattr(object, function_name, LogNormal())
        elif ((evolution == "exponential decay") and
                (not isinstance(function, Exponential))):
            setattr(object, function_name, Exponential())
        elif ((evolution == "user-defined") and
                (not isinstance(function, UserDefined))):
            setattr(object, function_name, UserDefined())

    def object_evolution_changed(self, info):
        if not hasattr(info.object, "evolution"):
            return None
        self.replace_evolution_function(info.object,
                                        info.object.evolution,
                                        info.object.function,
                                        "function")

    def object_d_evolution_changed(self, info):
        if not hasattr(info.object, "d_evolution"):
            return None
        self.replace_evolution_function(info.object,
                                        info.object.d_evolution,
                                        info.object.d_function,
                                        "d_function")

    def object_p_evolution_changed(self, info):
        if not hasattr(info.object, "p_evolution"):
            return None
        self.replace_evolution_function(info.object,
                                        info.object.p_evolution,
                                        info.object.p_function,
                                        "p_function")
