#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
ControlPanel class and auxiliary stuff.
This is the main GUI window.
"""

import sys
import os
import time
from datetime import timedelta
from threading import Thread
import json

from .cellfile import CellFile
from .morphogen import Morphogen
from .boundarycondition import (BoundaryCondition, LogNormal, Exponential,
                                Constant, UserDefined)
from .transport import (UniformDiffusion, SimpleUnidirectionalActiveTransport,
                        SmithModel2006, ImposedCellConcentrations)
from .plottingcontrol import PlottingControl

from traits.api import (HasTraits, Instance, Str, Int, Float, Bool, List, Enum,
                        File, Button)
from traitsui.api import (View, Item, Group, HGroup, TextEditor, FileEditor,
                          ListEditor, CheckListEditor, EnumEditor, OKButton,
                          CancelButton)

time_units = {
    "second": 1,
    "minute": 60,
    "hour": 60 * 60,
    "day": 24 * 60 * 60}


class JSONCodec():
    """Serialization and deserialization methods.

    Methods intended to be called from within 'json' module methods for saving
    and loading settings.

    """

    def __init__(self):

        # class_dict specifies which instance types will be serialized
        self.class_dict = {
            ControlPanel: "ControlPanel",
            Morphogen: "Morphogen",
            BoundaryCondition: "BoundaryCondition",
            LogNormal: "LogNormal",
            Exponential: "Exponential",
            Constant: "Constant",
            UserDefined: "UserDefined",
            UniformDiffusion: "UniformDiffusion",
            SmithModel2006: "SmithModel2006",
            SimpleUnidirectionalActiveTransport:
                "SimpleUnidirectionalActiveTransport",
            ImposedCellConcentrations: "ImposedCellConcentrations"
            }

    def coder(self, obj):
        if obj.__class__ in self.class_dict:
            traits_dict = obj.get()
            traits_dict['__class__'] = self.class_dict[obj.__class__]
            return traits_dict

    def decoder(self, json_obj):
        if "__class__" in json_obj:
            if json_obj["__class__"] == "ControlPanel":
                del json_obj["__class__"]
                return json_obj
            # the following elif blocks are here for backward campatibility
            elif json_obj["__class__"] == "DiffusiveSignal":
                del json_obj["__class__"]
                D = json_obj["d_intracell"]
                d = -json_obj["alpha"]
                del json_obj["d_intracell"]
                del json_obj["d_intercell"]
                del json_obj["alpha"]
                del json_obj["P"]
                obj = Morphogen()
                obj.set(**json_obj)
                obj.transport_mechanism = "uniform diffusion"
                obj.transport = UniformDiffusion()
                obj.transport.D = D
                obj.transport.d_function = Constant(constant_value=d)
                return obj
            elif json_obj["__class__"] == "UniformDiffusion" \
                    and "d" in json_obj:
                del json_obj["__class__"]
                d = json_obj["d"]
                del json_obj["d"]
                obj = UniformDiffusion()
                obj.set(**json_obj)
                obj.d_function = Constant(constant_value=d)
                return obj
            elif json_obj["__class__"] == \
                    "SimpleUnidirectionalActiveTransport" and "p" in json_obj:
                del json_obj["__class__"]
                p = json_obj["p"]
                del json_obj["p"]
                obj = SimpleUnidirectionalActiveTransport()
                obj.set(**json_obj)
                obj.p_function = Constant(constant_value=p)
                return obj
            for key, value in self.class_dict.items():
                if json_obj["__class__"] == value:
                    del json_obj["__class__"]
                    obj = key()
                    obj.set(**json_obj)
                    return obj
            return json_obj


class SettingsFile(HasTraits):
    """GUI for selecting a settings file to load."""
    file_name = File(os.getcwd())
    file_editor = FileEditor(filter=["*.json"])

    # Display specification (one Item per editor style):
    file_group = Group(
        Item('file_name',
             style='custom',
             show_label=False,
             editor=file_editor),
        Item('_'),
        Item('file_name', style='text', show_label=False),
        )

    def _ConcelButton_fired(self):
        pass  # TODO

    traits_view = View(
        file_group,
        kind='modal',
        title="Select a file",
        buttons=[OKButton, CancelButton],
        resizable=True,
        height=0.6, width=0.4
        )


class TextDisplay(HasTraits):
    """Used for displaying the state of a simulation run."""
    string = Str()

    view = View(Item('string',
                     show_label=False,
                     editor=TextEditor(multi_line=False),
                     style='custom'))


def growth(simu):
    """Simulation run.

    simu : ControlPanel instance
    """
    start_time = timedelta(days=simu.start_time)
    stop_time = timedelta(days=simu.stop_time)
    activation_time = timedelta(days=simu.activation_time)
    simu.cell_file = CellFile(
        nb_cells=simu.nb_cells,
        initial_CRD=simu.cell_diameter,
        division_threshold=simu.division_threshold,
        enlargement_threshold=simu.enlargement_threshold,
        growth_prefactor=simu.growth_prefactor,
        size_dependent_strains=simu.size_dependent_strains,
        data_time_unit=time_units["day"]
        )
    division_identity_signal = simu.signal_from_name(
            simu.division_identity_signal)
    enlargement_identity_signal = simu.signal_from_name(
            simu.enlargement_identity_signal)
    growth_control_signals = [simu.signal_from_name(name)
                              for name in simu.growth_control_signals]
    simu.cell_file.grow(
        start_time,
        stop_time,
        activation_time=activation_time,
        input_time_step=simu.input_time_step,
        growth_time_step=simu.growth_time_step,
        snapshot_time_step=simu.snapshot_time_step,
        nodes_per_cell=simu.nodes_per_cell,
        division_identity_signal=division_identity_signal,
        enlargement_identity_signal=enlargement_identity_signal,
        control_signals=growth_control_signals
        )


class GrowthThread(Thread):
    """Thread of a simulation run."""
    def run(self):
        self.display.string = "Simulation running..."
        simu = self.simulation
        simu.plotting_control = None
        start = time.time()
        growth(simu)
        time_elapsed = time.time() - start
        self.display.string = "Simulation finished after "\
                              "{0:.1f} s.".format(time_elapsed)
        simu.plotting_control = PlottingControl(
            simu.cell_file, simu.start_time, simu.stop_time,
            activation_time=simu.activation_time)


class ControlPanel(HasTraits):
    """This object is the main window of the interface.

    Its view is composed of tabs:
    1) The simulation / drawing tab, implemented directly in this object.
    2) The model tab (Model object), for defining the model.
    3) The signal tab (a List of Morphogen objects), for parametrizing
    the signals.

    """

    # List of signals
    signals = List(trait=Morphogen)
    # List is signal names
    signal_names = List(Str)

    def __init__(self, nb_signals=1, settings_file=None):
        signals = []
        for i in range(nb_signals):
            signals.append(Morphogen(number=i+1))
        self.signals = signals
        self.signal_names = [signal.name for signal in self.signals]
        if settings_file is not None:
            self.set_from_file(settings_file)

    def signal_from_name(self, name):
        """From a name, return the corresponding signal."""
        L = [signal for signal in self.signals if signal.name == name]
        if L:
            return L[0]
        else:
            return None

    ##################
    # Simulation tab #
    ##################

    start_time = Int(
        0,
        label="Start (day of year)",
        desc="the first day of simulation"
        )

    activation_time = Int(
        0,
        label="Cambium activation (day of year)",
        desc="the date of cambium activation"
        )

    stop_time = Int(
        30,
        label="Stop (day of year)",
        desc="the last day of simulation"
        )

    nb_cells = Int(
        4,
        label="Initial number of cells"
        )

    cell_diameter = Float(
        6.,
        label="Initial cell diameter (µm)"
        )

    simulation_mode = Enum(
        "non-equilibrium",
        "quasistatic",
        label="Simulation mode",
        desc="if the full non-equilibrium reaction-diffusion is to be "
             "solved or if the quasistatic approximation can be made."
        )

    launch_simulation = Button("Run")
    display = Instance(TextDisplay, ())
    display.string = ""
    growth_thread = Instance(GrowthThread)

    # GUI for plotting outputs
    plotting_control = Instance(PlottingControl)

    def _launch_simulation_fired(self):
        """Callback of the "launch simulation" button.

        This starts the simulation thread, or kills it.
        """
        if self.growth_thread and self.growth_thread.is_alive():
            self.growth_thread.wants_abort = True
        else:
            self.growth_thread = GrowthThread()
            self.growth_thread.wants_abort = False
            self.growth_thread.display = self.display
            self.growth_thread.simulation = self
            self.growth_thread.start()

    json_codec = Instance(JSONCodec, ())
    settings_file = Instance(SettingsFile, ())
    save_settings = Button("Save settings")
    load_settings = Button("Load settings")

    def set_from_file(self, file_name):
        """Set traits from a configuration file."""
        with open(file_name, "r") as f:
            traits_dict = json.load(f, object_hook=self.json_codec.decoder)
            keys_to_remove = []
            for key, value in traits_dict.items():
                if value is None:
                    keys_to_remove.append(key)
                # The next two lines are for backward compatibility.
                if key == "simulation_mode" and "non-equilibrium" in value:
                    traits_dict[key] = "non-equilibrium"
            for key in keys_to_remove:
                del traits_dict[key]
            # The following if-block is for backward compatibility.
            if "identity_signal" in traits_dict:
                identity_signal = traits_dict["identity_signal"]
                traits_dict["division_identity_signal"] = identity_signal
                traits_dict["enlargement_identity_signal"] = identity_signal
                del traits_dict["identity_signal"]
            self.set(**traits_dict)

    def save_to_file(self, file_name):
        """Save current settings into a settings file."""
        with open(file_name, 'w') as f:
            json.dump(self, f, indent=4, default=self.json_codec.coder)

    def _load_settings_fired(self):
        """Callback of the "load settings" button.

        This opens a window for selecting a settings file and loads settings
        from it.
        """
        self.settings_file.edit_traits()
        file_name = self.settings_file.file_name
        if not file_name or os.path.isdir(file_name):
            return False
        if os.path.isfile(file_name):
            self.set_from_file(file_name)
        else:
            raise IOError("Configuration file {} does not "
                          "exist!".format(file_name))

    def _save_settings_fired(self):
        """Callback of the "load settings" button.

        This opens a window for selecting a settings file and saves settings
        into it.
        """
        output = self.settings_file.edit_traits()
        if output:
            file_name = self.settings_file.file_name
            if not file_name or file_name == os.getcwd():
                return False
            if not file_name[-5:] == ".json":
                file_name += ".json"
                self.settings_file.file_name = file_name
            self.save_to_file(file_name)

    #############
    # Model tab #
    #############

    division_threshold = Float(
        1,
        label="Division threshold",
        desc="a threshold on the signal concentration. Above "
             "threshold, cell division can occur when the cell reach "
             "the critical size."
        )

    enlargement_threshold = Float(
        0.5,
        label="Enlargement threshold",
        desc="a threshold on the signal concentration. Under "
             "this threshold, enlargement ceases."
        )

    threshold_list = [division_threshold, enlargement_threshold]

    division_identity_signal = Str(
        label="Division identity signal",
        desc="the signal which determines which cells have a division identity."
        )

    def _division_identity_signal_default(self):
        if len(self.signals) > 0:
            return self.signals[0].name
        else:
            return None

    enlargement_identity_signal = Str(
        label="Enlargement identity signal",
        desc="the signal which determines which cells have a enlargement identity."
        )

    def _enlargement_identity_signal_default(self):
        if len(self.signals) > 0:
            return self.signals[0].name
        else:
            return None

    growth_control_signals = List(
        trait=Str,
        label="Growth control signals",
        desc="the signals which determine the growth rates of cells."
        )

    def _growth_control_signals_default(self):
        if len(self.signals) > 0:
            return [self.signals[0].name]
        else:
            return []

    def _signals_items_changed(self, old, new):
        """Callback after removal or addition of signals to 'signals'.

        Any signal removed from the 'signals' List must be also removed from
        the 'growth_control_signals' List if needed. If the removed signal is
        an identity_signal, tnen the 'division_identity_signal' or the
        'enlargement_identity_signal' must be changed.

        """
        if self.division_identity_signal is None and len(new.added) > 0:
            self.division_identity_signal = self.signals[0].number
        if self.enlargement_identity_signal is None and len(new.added) > 0:
            self.enlargement_identity_signal = self.signals[0].number
        if len(new.removed) > 0:
            if not self.signals:
                self.division_identity_signal = None
                self.enlargement_identity_signal = None
            for signal in new.removed:
                if self.division_identity_signal is signal.number:
                    self.division_identity_signal = self.signals[0].number
                if self.enlargement_identity_signal is signal.number:
                    self.enlargement_identity_signal = self.signals[0].number
                for growth_control_signal in self.growth_control_signals:
                    if growth_control_signal is signal.number:
                        self.growth_control_signals.remove(
                            growth_control_signal)
        self.signal_names = [signal.name for signal in self.signals]

    growth_prefactor = Float(
        0.05,
        label="Growth prefactor",
        desc="the conversion factor between signal concentration and "
             "growth rate. Unit: day¯¹."
        )

    add_signal = Button("Add signal")

    def _add_signal_fired(self):
        """Callback of the "add signal" button.

        Create a new signal and add it to the 'signals' List trait.
        The new signal is consistently numbered.

        """
        if len(self.signals) == 0:
            number = 1
        else:
            number = self.signals[-1].number + 1
        self.signals.append(Morphogen(number=number))

    size_dependent_strains = Bool(False,
        label="Size-dependent strains",
        desc="whether strains are inversely proportional to cell diameters."
        )

    ################
    # Advanced tab #
    ################

    nodes_per_cell = Int(
        3,
        label="Mesh nodes per cell",
        desc="the initial number of mesh nodes per cell for PDE solving."
        )

    input_time_step = Float(
        time_units["hour"],
        label="Input time step (seconds)",
        desc="the time step between input value updates"
        )

    growth_time_step = Float(
        time_units["hour"],
        label="Growth time step (seconds)",
        desc="the time step for simulating growth. It is different from the "
             "diffusion time step, which is set based on the "
             "Courant–Friedrichs–Lewy condition and is usually much shorter."
        )

    snapshot_time_step = Float(
        time_units["hour"],
        label="Snapshot time step (seconds)",
        desc="the time step between snapshots saved."
        )

    # View

    traits_view = View(
        Group(
            # Simulation tab
            Group(
                Group(
                    Item('start_time'),
                    Item('activation_time'),
                    Item('stop_time'),
                    Item('nb_cells'),
                    Item('cell_diameter')
                    ),
                Item('launch_simulation', show_label=False),
                Item('display', style='custom', show_label=False),
                Item('plotting_control',
                     style='custom',
                     show_label=False),
                HGroup(
                    Item('load_settings', show_label=False),
                    Item('save_settings', show_label=False)
                    ),
                label="Simulation",
                dock='tab'
                ),
            # Model tab
            Group(
                Item('division_threshold'),
                Item('enlargement_threshold'),
                Item('growth_prefactor'),
                Item('division_identity_signal',
                     editor=EnumEditor(name='signal_names'),
                     style="simple"),
                Item('enlargement_identity_signal',
                     editor=EnumEditor(name='signal_names'),
                     style="simple"),
                Item('growth_control_signals',
                     editor=CheckListEditor(name='signal_names', cols=4),
                     style="custom"),
                Item('add_signal', show_label=False),
                Item('size_dependent_strains'),
                label="Model",
                dock='tab'
                ),
            # Signals tab
            Item('signals',
                 style='custom',
                 show_label=False,
                 editor=ListEditor(
                     use_notebook=True,
                     deletable=True,
                     page_name='.name',
                     dock_style='tab'
                     ),
                 dock='tab'
                 ),
            # Advanced tab
            Group(
                Item('nodes_per_cell'),
                Item('input_time_step'),
                Item('growth_time_step'),
                Item('snapshot_time_step'),
                label="Advanced",
                dock='tab'
                ),
            layout='tabbed',
            ),
        title="XyDyS",
        resizable=True,
        height=0.6, width=0.3
        )

    def growth(self):
        """Launch a simulation run.

        Useful for command-line interface.
        """
        growth(self)
