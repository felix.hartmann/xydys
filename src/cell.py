#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Cell objects simulate cells resulting from cambial activity.
"""

class Cell(object):
    """Cell class."""
    def __init__(self, index=None, date=None, diameter=None):
        self.indices = []
        self.dates = []
        self.diameters = []
        self.identities = []
        self.strain_rates = []
        if index is not None:
            self.indices.append(index)
        if date is not None:
            self.dates.append(date)
        if diameter is not None:
            self.diameters.append(diameter)

    def get_identity(self, time):
        if time not in self.dates:
            return None
        else:
            i = self.dates.index(time)
            return self.identities[i]
