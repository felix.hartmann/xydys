#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Module containing functions for visualizing a CellFile object and its evolution.
"""

import numpy as np
from math import factorial
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from matplotlib.lines import Line2D
from matplotlib.patches import Rectangle
from matplotlib.patches import Arrow


def savitzky_golay(y, window_size, order=1, deriv=0, rate=1):
    r"""Smooth (and optionally differentiate) data with a Savitzky-Golay filter.

    The Savitzky-Golay filter removes high frequency noise from data. It has the advantage
    of preserving the original shape and features of the signal better than other types of
    filtering approaches, such as moving averages techniques.

    Parameters
    ----------
    y : array_like, shape (N,)
        the values of the time history of the signal.
    window_size : int
        the length of the window. Must be an odd integer number.
    order : int
        the order of the polynomial used in the filtering.
        Must be less then `window_size` - 1.
        (default=1 means simple moving average)
    deriv: int
        the order of the derivative to compute
        (default=0 means smoothing only)

    Returns
    -------
    ys : ndarray, shape (N)
        the smoothed signal (or it's n-th derivative).

    Notes
    -----
    The Savitzky-Golay is a type of low-pass filter, particularly suited for smoothing
    noisy data. The main idea behind this approach is to make for each point a
    least-square fit with a polynomial of high order over a odd-sized window centered at
    the point.

    Examples
    --------
    t = np.linspace(-4, 4, 500)
    y = np.exp( -t**2 ) + np.random.normal(0, 0.05, t.shape)
    ysg = savitzky_golay(y, window_size=31, order=4)
    import matplotlib.pyplot as plt
    plt.plot(t, y, label='Noisy signal')
    plt.plot(t, np.exp(-t**2), 'k', lw=1.5, label='Original signal')
    plt.plot(t, ysg, 'r', label='Filtered signal')
    plt.legend()
    plt.show()

    References
    ----------
    .. [1] A. Savitzky, M. J. E. Golay, Smoothing and Differentiation of
       Data by Simplified Least Squares Procedures. Analytical
       Chemistry, 1964, 36 (8), pp 1627-1639.
    .. [2] Numerical Recipes 3rd Edition: The Art of Scientific Computing
       W.H. Press, S.A. Teukolsky, W.T. Vetterling, B.P. Flannery
       Cambridge University Press ISBN-13: 9780521880688

    This code is from the SciPy cookbook:
    http://wiki.scipy.org/Cookbook/SavitzkyGolay
    """
    try:
        window_size = np.abs(int(window_size))
        order = np.abs(int(order))
    except ValueError:
        raise ValueError("window_size and order have to be of type int")
    if window_size < order + 2:
        # window_size is too small for the polynomials order
        return y
    if window_size % 2 != 1:
        # window_size size must be an odd number
        window_size += 1
    order_range = range(order + 1)
    half_window = (window_size - 1) // 2
    # precompute coefficients
    b = np.mat([[k**i for i in order_range]
                for k in range(-half_window, half_window + 1)])
    m = np.linalg.pinv(b).A[deriv] * rate**deriv * factorial(deriv)
    # pad the signal at the extremes with
    # values taken from the signal itself
    firstvals = y[0] - np.abs(y[1:half_window+1][::-1] - y[0])
    lastvals = y[-1] + np.abs(y[-half_window - 1:-1][::-1] - y[-1])
    y_padded = np.concatenate((firstvals, y, lastvals))
    z = np.convolve(m[::-1], y_padded, mode='valid')
    return z


def resample_uniformly(X, Y, dx=None):
    """Resample array Y based on X, but with constant stepwidth."""
    if dx is None:
        # Finest sampling by default.
        dx = min(X[1:] - X[:-1])
    nb_samples = int((X[-1] - X[0])/dx) + 1
    resampled_Y = np.empty(nb_samples)
    cur_x = X[0]
    i = 0
    while i < nb_samples:
        idx = np.searchsorted(X, cur_x)
        if idx == len(X):
            resampled_Y[i] = Y[idx-1]
        elif abs(cur_x - X[idx]) <= abs(cur_x - X[idx-1]):
            resampled_Y[i] = Y[idx]
        else:
            resampled_Y[i] = Y[idx-1]
        cur_x += dx
        i += 1
    return resampled_Y


######################################
# Utilities for drawing a cell file. #
######################################

def init_cell_file_drawing(
        xlim,
        ylim,
        fixed_cambium_right=True,
        fontsize="x-large",
        figsize=None,
        figure=None,
        axis=None):
    """
    Create and return a Figure object and an Axes object ready for plotting
    the cell file. If a Figure or Axis is given as parameter, it will be used
    instead.

    Parameters
    ----------
    xlim : tuple of two floats
        Limits of the x-axis: ax1.set_xlim(*xlim).
    ylim : tuple of two floats
        Limits of the y-axis: ax1.set_ylim(*ylim).
    figure : Figure instance, default: None
        If not None, figure is used for creating Axes, and is return as first
        element of the output tuple.
    axis : Axis instance, default: None
        If not None, axis is used for plotting and is returned as second
        element of the output tuple.

    See the draw_cell_file method for definitions of other parameters.

    Outputs : figure, axis
    """
    if axis is None:
        if figure is None:
            figure, axis = plt.subplots(figsize=figsize)
        else:
            axis = figure.add_subplot(111)
    axis.xaxis.set_ticks_position('bottom')
    axis.spines['top'].set_visible(False)
    axis.set_xlim(*xlim)
    axis.set_ylim(*ylim)
    axis.set_xlabel(r"$\mu m$", size=fontsize)
    ylabel = "Signal concentration"
    if fixed_cambium_right:
        axis.invert_xaxis()
        axis.yaxis.set_ticks_position('left')
        axis.spines['right'].set_visible(False)
    else:
        axis.spines['left'].set_visible(False)
        axis.yaxis.set_label_position("right")
        axis.yaxis.tick_right()
    axis.set_ylabel(ylabel, size=fontsize)
    for label in axis.get_xticklabels() + axis.get_yticklabels():
        label.set_fontsize(fontsize)
    return figure, axis


def display_cell_indices(
        axis,
        x_cell_centers,
        t_diameter,
        digit_relative_shift=0.25,
        xmax=None,
        fontsize="x-large"):
    """Display cell indices on the cells in the drawing of the cell file.

    Parameters
    ----------
    axis : Axis instance
        Axis instance on which cell indices are written.
    x_cell_centers : 1-D array
        Position of cell centers along the x axis.
    t_diameter : float
        Transverse diameter of the cells.
    xmax : float
        Maximal x coordinate of indexed cells.
    fontsize : int or str, optional, default: "x-large"
        Size argument for text.
    """
    initial_CRD = 2 * x_cell_centers[0]
    if xmax is not None:
        x_cell_centers = x_cell_centers[x_cell_centers < xmax]
    for i, xpos in enumerate(x_cell_centers, 1):
        nb_digits = 1 + int(np.log10(i))
        xpos += nb_digits * digit_relative_shift * initial_CRD
        axis.text(xpos, 1.6, str(i), fontsize=fontsize)
    return axis


def fill_cell_file_drawing(
        cell_file,
        axis,
        time,
        xlim,
        signal=True,
        show_transport_polarity="one",
        arrow_stem=10,
        arrow_width=0.25,
        cell_concentrations=None,
        threshold_list=None,
        strain_rates=None,
        colored_zones=False,
        cell_indices=False,
        tangential_diameter=None,
        date=False,
        colors=['b', 'r', 'g', 'm', 'c'],
        linewidth=1,
        fixed_cambium_right=True,
        fontsize="x-large"):
    """Clear axes and plot the cell file.

    Parameters
    ----------
    cell_file : CellFile instance
        The cell file from which diameters are plotted.
    axis : Pyplot Axes instance
        Axis on which the cell file is plotted.

    See the draw_cell_file method for definitions of other parameters.

    Output : axis
    """
    cf = cell_file
    for line in axis.get_lines():
        line.remove()
    for patch in reversed(axis.patches):
        patch.remove()
    nodes_per_cell, x, D, u = cf.values_for_drawing(time)
    nb_cells, nb_signals = len(D), len(u)
    rectangle_positions = D.cumsum() - D
    x_cell_centers = rectangle_positions + D/2
    if tangential_diameter is None:
        t_diameter = 2 * cf.concentration_scale()
    else:
        t_diameter = tangential_diameter
    # Cells are drawn as rectangles.
    rects = []
    for i, r_diameter in enumerate(D):
        rects.append(
            Rectangle((rectangle_positions[i], 0),
                      r_diameter, t_diameter, linewidth=linewidth,
                      facecolor='w', edgecolor='k', alpha=0.5)
            )
    if colored_zones:
        if cf.identity_definition == "gradient":
            conc_operator = cf.compute_concentration_operator(nodes_per_cell)
            cell_conc = cf.get_cell_concentrations(u, conc_operator)
            zone_1, zone_2, zone_3 = cf.get_gradient_zones(cell_conc)
        elif cf.identity_definition == "anatomy":
            zone_1, zone_2, zone_3 = cf.get_cell_zones_anatomy(time)
        for i in zone_1:
            try:
                rects[i].set_facecolor('green')
            except IndexError:  # that error is mysterious
                pass
        for i in zone_2:
            rects[i].set_facecolor('royalblue')
        for i in zone_3:
            try:
                rects[i].set_facecolor('red')
            except IndexError:  # that error is mysterious
                pass
    # If a signal has a polar transport, arrows are drawn on the membranes.
    # (only for simple unidirectional polar transport)
    arrows = []
    if show_transport_polarity is not None:
        for i, mechanism in enumerate(cf.transport_mechanisms):
            if mechanism == "simple unidirectional active transport":
                y_pos = 0.8 * t_diameter
                color = colors[i % nb_signals]
                orientation = cf.transports[i].carrier_orientation
                if orientation == "toward the xylem":
                    polarity = 1
                elif orientation == "toward the cambium":
                    polarity = -1
                # an arrow on each membrane
                if show_transport_polarity == "each":
                    for x_pos in rectangle_positions[1:]:
                        arrows.append(Arrow(x_pos - polarity * arrow_stem/2, y_pos,
                                            polarity * arrow_stem, 0, width=arrow_stem,
                                            color=color))
                # an arrow on one well-selected membrane
                elif show_transport_polarity == "one":
                    zone_1, zone_2, zone_3 = cf.get_cell_zones_anatomy(time)
                    if zone_2:
                        cell_index = zone_2[len(zone_2)//2]
                    else:
                        cell_index = nb_cells // 2
                    x_pos = rectangle_positions[cell_index]
                    arrows.append(Arrow(x_pos - polarity * arrow_stem/2, y_pos,
                                        polarity * arrow_stem, 0, width=arrow_width,
                                        color=color))
                # an arrow on given membranes
                else:
                    if not np.iterable(show_transport_polarity):
                        show_transport_polarity = [show_transport_polarity]
                    for index in show_transport_polarity:
                        x_pos = rectangle_positions[index]
                        arrows.append(Arrow(x_pos - polarity * arrow_stem/2, y_pos,
                                            polarity * arrow_stem, 0, width=arrow_width,
                                            color=color))
    for patch in rects + arrows:
        axis.add_patch(patch)
    # Add a wider line at the boundary of the file so that it is visible.
    axis.plot([0, 0], [0, t_diameter], 'k-', linewidth=2*linewidth)
    # Signal concentration and thresholds are plotted on the same axis.
    if signal:
        for i, mechanism in enumerate(cf.transport_mechanisms):
            concentrations = u[i].flatten(order='F')
            color = colors[i % nb_signals]
            if mechanism == "uniform diffusion":
                axis.plot(x, concentrations, color + "o")
            else:
                axis.plot(x_cell_centers, concentrations, color + "o")
    if cell_concentrations is not None:
        conc_operator = cf.compute_concentration_operator(nodes_per_cell)
        cell_conc = cf.get_cell_concentrations(u, conc_operator)
        for i in cell_concentrations:
            C = cell_conc[i]
            axis.plot(x_cell_centers, C, colors[i % nb_signals] + "o")
    if threshold_list is not None:
        for i, threshold in enumerate(threshold_list):
            threshold_line = Line2D(xlim, (threshold, threshold),
                                    color=colors[i % nb_signals], linestyle='--')
            threshold_line.set_linewidth(2*threshold_line.get_linewidth())
            axis.add_line(threshold_line)
    if strain_rates is not None:
        S = 1.05 * t_diameter * strain_rates
        axis.plot(x_cell_centers, S, 'ko')
    if cell_indices:
        display_cell_indices(axis, x_cell_centers, t_diameter, fontsize=fontsize)
    if date:
        day = cf.date_list[time].date()
        daystr = day.strftime("%d %B").decode('utf8')
        axis.annotate(
                daystr,
                xy=(.5, .95), xycoords='axis fraction',
                fontsize="medium",
                horizontalalignment='center',
                verticalalignment='center')
    return axis


def draw_cell_file(
        cell_file,
        time=-1,
        signal=True,
        show_transport_polarity="one",
        arrow_stem=10,
        arrow_width=0.25,
        cell_concentrations=None,
        thresholds=True,
        strain_rates=False,
        colored_zones=True,
        cell_indices=False,
        tangential_diameter=None,
        date=False,
        colors=['b', 'r', 'g', 'm', 'c'],
        linewidth=1,
        fontsize="x-large",
        xlim=None,
        ylim=None,
        fixed_cambium_right=True,
        figsize=None,
        figure=None,
        axis=None,
        save=False,
        base_name=""):
    """Draw the cells of a radial file.

    Parameters
    ----------
    cell_file : CellFile instance
        The cell file from which diameters are plotted.
    time : int, optional, default: -1
        Time in the growth process at which the file is drawn.
        Default is the last time step (-1).
    signal : bool, optional, default: True
        If True (default), signal concentrations are drawn.
    transport_polarity : [None, "each", "one", int], optional, default: "one"
        If not None, the polarity of a transport mechanism is pictured with arrows.
        "each": an arrow on each membrane
        "one": an arrow on a well-selected membrane
        int: index of the cell where the arrow is placed
        For now, it is only for the simple unidirectional polar transport.
    arrow_stem : float, optional, default: 10
        Stem length of the arrows picturing transport polarity.
    arrow_width : float, optional, default: 0.25
        Stem width of the arrows picturing transport polarity.
    cell_concentrations : list of ints, optional, default: None
        Indices of signals whose cell concentrations (mean concentrations within a cell)
        will be drawn.
    thresholds : list, optional, default: None
        List of thresholds (division_threshold, enlargement_threshold...) to be drawn as
        2DLines.
    strain_rates : Boolean, optional, default: False
        If True, plots strain rates.
    colored_zones : Boolean, optional, default: True
        If True, cells are filled with colors depending on the zone they belong to.
    cell_indices : Boolean, optional, default: False
        If True, the index of each cell is displayed on it.
    tangential_diameter : float, optional, default: None
        Tangential diameter of the cells, only for representation purpose.
        If None, set automatically.
    date : Boolean, optional, default: False
        If True, writes current date on the plot.
    colors : list of colors, optional, default: ['b', 'r', 'g', 'm', 'c']
        List of colors in which signal concentrations will be drawn.
    linewidth : float, optional, default: 1
        Linewidth argument in drawing cells as rectangles.
    fontsize : int or str, optional, default: "x-large"
        Size argument for labels.
    xlim : tuple of two floats, optional, default: None
        Limits of the x-axis: ax.set_xlim(*xlim).
    ylim : tuple of two floats, optional, default: None
        Limits of the y-axis: ax.set_ylim(*ylim).
    fixed_cambium_right : Boolean, optional, default: True
        If True, the cambium is fixed on the right side of the plot.
    figsize : tuple of two floats, optional, default: None
        Size of the figure (figsize argument in plt.subplot).
    figure : Figure instance, default: None
        If not None, figure is used for creating Axes, and is return as first element of
        the output tuple.
    axis : Axis instance, default: None
        If not None, axis is used for plotting and is returned as second element of the
        output tuple.
    save : bool, optional, default: False
        If True, the output figure is saved.
    base_name : str, optional, default: ""
        Base name (path) of the file if the figure is saved.

    Output : If the 'save' parameter evaluates to False, returns a Figure instance and an
             Axis instance.
    """
    cf = cell_file
    if axis is None:
        if figure is None:
            figure, axis = plt.subplots(figsize=figsize)
        else:
            axis = figure.add_subplot(111)
    if thresholds:
        threshold_list = (cf.division_threshold, cf.enlargement_threshold)
    else:
        threshold_list = None
    if xlim is None:
        xlim = (0, 1.01 * cf.length(time))
    if ylim is None:
        if thresholds:
            ylim = (0, 2.2 * cf.division_threshold)
        else:
            ylim = (0, 2.2)
    figure, axis = init_cell_file_drawing(
        xlim, ylim,
        fixed_cambium_right=fixed_cambium_right,
        fontsize=fontsize, figsize=figsize,
        figure=figure, axis=axis)
    if strain_rates:
        S = cf.S_history[time].copy()
        S_max = S.max()
        if S_max == 0:
            S_max = 1
        S /= S_max
    else:
        S = None
    axis = fill_cell_file_drawing(
            cf,
            axis,
            time,
            xlim,
            signal=signal,
            show_transport_polarity=show_transport_polarity,
            arrow_stem=arrow_stem,
            arrow_width=arrow_width,
            cell_concentrations=cell_concentrations,
            threshold_list=threshold_list,
            colored_zones=colored_zones,
            cell_indices=cell_indices,
            tangential_diameter=tangential_diameter,
            strain_rates=S,
            date=date,
            colors=colors,
            linewidth=linewidth)
    figure.tight_layout()
    if save:
        figure.savefig("{0}{1}.png".format(base_name, time))
        plt.close(figure)
    else:
        return figure, axis


########################################################
# Utilities for drawing the development of a cell file #
# using extrusion in space-time.                       #
########################################################

# Drawing inspiration from
# Hammel, M. and Prusinkiewicz, P. (1996).
# Visualization of Developmental Processes by Extrusion in Space-time.
# In Proceedings of the Conference on Graphics Interface '96, GI '96,
# page 246–258,
# Toronto, Ont., Canada, Canada.
# Canadian Information Processing Society.

def match_cell_boundaries_within_an_extrusion_layer(initial_diameters,
                                                    final_diameters):
    """Match cell boundaries across an extrusion layer.

    Parameters
    ----------
    initial_diameters: 1-D array of floats
        Cell diameters at the beginning of the extrusion layer (at time t).
    final_diameters: 1-D array of floats
        Cell diameters at the end of the extrusion layer (at time t+1).

    Returns
    -------
    boundary_pairs: list of tuples (float, float)
    """
    if len(initial_diameters) == len(final_diameters):
        # No division occured between t and t+1.
        initial_Dcum = initial_diameters.cumsum()
        final_Dcum = final_diameters.cumsum()
        initial_boundaries = initial_Dcum - initial_diameters
        final_boundaries = final_Dcum - final_diameters
        initial_boundaries = np.append(initial_boundaries, initial_Dcum[-1])
        final_boundaries = np.append(final_boundaries, final_Dcum[-1])
        return list(zip(initial_boundaries, final_boundaries))
    else:
        # Try to guess which cells divided.
        boundary_pairs = []
        initial_boundary, final_boundary = 0, 0
        boundary_pairs.append((initial_boundary, final_boundary))
        final_index = 0
        for initial_index, initial_diameter in enumerate(initial_diameters):
            initial_boundary += initial_diameter
            final_diameter = final_diameters[final_index]
            if final_diameter >= initial_diameter:
                # No division here.
                final_index += 1
                final_boundary += final_diameter
            else:
                # A division occured.
                # Sum up diameters of both daughter cells to get the final
                # boundary of the mother cell.
                final_boundary += final_diameter \
                    + final_diameters[final_index+1]
                # Skip the daughter cells.
                final_index += 2
            boundary_pairs.append((initial_boundary, final_boundary))
        return boundary_pairs


def draw_extruded_cell_file(
        cell_file,
        start=None,
        stop=None,
        step=None,
        linewidth=1,
        signals=[],
        colormaps=['Blues', 'Reds', 'Greens'],
        interpolation='gaussian',
        filterrad=4,
        vmax=[],
        alpha=1,
        colored_zones=False,
        fontsize="x-large",
        xlim=None,
        fixed_cambium_right=True,
        figsize=None,
        figure=None,
        axis=None,
        save=False,
        base_name=""):
    """Draw the the development of a cell file by extrusion in space-time.

    Parameters
    ----------
    cell_file : CellFile instance
        The cell file from which diameters are plotted.
    start : int
        Time (day of year) in the growth process from which the file is drawn.
    stop : int
        Time (day of year) in the growth process until which the file is drawn.
    step : int, default: None
        Step (in seconds) between two consecutives layers in the extrusion.
        If None, step = cell_file.conversion.
    linewidth : float, optional, default: 1
        Linewidth argument in drawing cell boundaries.
    signals : list of ints (optionaly a single int), optional, default: []
        Indices of the signals to draw.
        If None, no signal concentration is drawn.
    colormaps : list of Matplotlib colormaps, optional.
        Default: ['Blues', 'Reds', 'Greens']
        List of colors in which signal concentrations will be drawn.
    interpolation : string, default: 'gaussian'
        'interpolation' parameter when drawing signal distribution using
        imshow.
    filterrad : float, default: 4
        'filterrad' parameter when drawing signal distribution using imshow.
    vmax : float, default: None
        'vmax' value when drawing signal distribution using imshow.
        If None, set to two times the division threshold.
    alpha : float between 0 and 1, default: 1
        The alpha blending value when drawing signal distribution using imshow.
    colored_zones : Boolean, optional, default: True
        If True, cells are filled with colors depending on the zone they
        belong to. Ignored of 'signal' parameter is not None.
        Not implemented.
    fontsize : int or str, optional, default: "x-large"
        Size argument for labels.
    xlim : tuple of two floats, optional, default: None
        Limits of the x-axis: ax.set_xlim(*xlim).
    fixed_cambium_right : Boolean, optional, default: True
        If True, the cambium is fixed on the right side of the plot.
    figsize : tuple of two floats, optional, default: None
        Size of the figure (figsize argument in plt.subplot).
    figure : Figure instance, default: None
        If not None, figure is used for creating Axes, and is return as first
        element of the output tuple.
    axis : Axis instance, default: None
        If not None, axis is used for plotting and is returned as second
        element of the output tuple.
    save : bool, optional, default: False
        If True, the output figure is saved.
    base_name : str, optional, default: ""
        Base name (path) of the file if the figure is saved.

    Output : If the 'save' parameter evaluates to False, returns a Figure
             instance and an Axis instance.
    """
    cf = cell_file
    if isinstance(signals, int):
        # if 'signals' is an int, put it into a list
        signals = [signals]
    if isinstance(vmax, float):
        vmax = [vmax]
    if not vmax:
        vmax = [cf.get_max_concentration(signal) for signal in signals]
    # Turn lists into dictionaries, with signals as keys
    vmax_dict = {}
    for signal, v in zip(signals, vmax):
        vmax_dict[signal] = v
    vmax = vmax_dict
    colormaps_dict = {}
    for signal, cm in zip(signals, colormaps):
        colormaps_dict[signal] = cm
    colormaps = colormaps_dict 
    if axis is None:
        if figure is None:
            figure, axis = plt.subplots(figsize=figsize)
        else:
            axis = figure.add_subplot(111)
    if start is None:
        start = cf.start_time.days
    if stop is None:
        stop = cf.stop_time.days
    if step is None:
        step = int(cf.conversion)
    nb_times = int(cf.conversion * (stop - start + 1) / step)
    start_index = int(cf.conversion * (start - cf.start_time.days))
    stop_index = int(cf.conversion * (stop - cf.start_time.days))
    #max_length = cf.length(cf_stop)
    max_length = cf.length()
    if xlim is None:
        xlim = (0, 1.01 * max_length)
    ylim = (start, stop+1)
    axis.set_xlim(*xlim)
    axis.set_ylim(*ylim)
    if fixed_cambium_right:
        axis.invert_xaxis()
        axis.yaxis.set_ticks_position('left')
    else:
        axis.yaxis.set_label_position("right")
        axis.yaxis.tick_right()
    axis.set_xlabel(r"Distance from cambial initials ($\mu$m)", size=fontsize)
    axis.set_ylabel("Day of year", size=fontsize)
    for label in axis.get_xticklabels() + axis.get_yticklabels():
        label.set_fontsize(fontsize)
    for line in axis.get_lines():
        line.remove()
    lines = []
    for patch in reversed(axis.patches):
        patch.remove()
    dy = (ylim[1] - ylim[0]) / nb_times
    y_pos = ylim[0]
    initial_nb_nodes_per_cells, initial_x, initial_D, initial_u = \
        cf.values_for_drawing(start_index)
    dx = {}  # each signal has its own space step dx
    extruded_distributions = {}  # signal distributions to draw
    for signal in signals:
        #initial_u = initial_u[signal]
        mechanism = cf.transport_mechanisms[signal]
        # Let's choose the space discretization step dx for the visualization of
        # signal concentration.
        if mechanism == "uniform diffusion":
            # dx is the smallest interval between two nodes
            dx[signal] = min(initial_D/initial_nb_nodes_per_cells)
        elif mechanism == "simple unidirectional active transport":
            # dx is a fraction of the initial cell radial diameter
            dx[signal] = cf.initial_CRD / 8
        else:
            print("This transport mechanism is not implemented in space-time extrusion.")
            signal = None
        nb_cols = int(np.ceil(max_length/dx[signal]))
        extruded_distributions[signal] = np.zeros((nb_times, nb_cols))
    for i, t in enumerate(range(start_index, stop_index, step), 1):
        final_nb_nodes_per_cells, final_x, final_D, final_u = \
            cf.values_for_drawing(t)
        # Cell borders are drawn as lines.
        boundaries = match_cell_boundaries_within_an_extrusion_layer(initial_D, final_D)
        initial_left_boundary, final_left_boundary = boundaries[0]
        lines.append(Line2D([initial_left_boundary, final_left_boundary],
                            [y_pos, y_pos+dy],
                            color='k', linewidth=linewidth))
        for initial_right_boundary, final_right_boundary in boundaries[1:]:
            lines.append(Line2D([initial_right_boundary, final_right_boundary],
                                [y_pos, y_pos+dy],
                                color='k', linewidth=linewidth))
            initial_left_boundary, final_left_boundary = \
                initial_right_boundary, final_right_boundary
        # Signal distribution is represented with shades of green using imshow.
        # We need a uniform sampling of signal distribution tu use imshow.
        # The distribution is oversampled, using the min step in x.
        for signal in signals:
            mechanism = cf.transport_mechanisms[signal]
            # The array of signal concentration has to be resampled with the step dx.
            if mechanism == "uniform diffusion":
                resampled_u = resample_uniformly(
                        initial_x, initial_u[signal], dx=dx[signal])
            elif mechanism == "simple unidirectional active transport":
                cell_borders = initial_D.cumsum() - initial_D
                x_cell_centers = cell_borders + initial_D/2
                resampled_u = resample_uniformly(
                        x_cell_centers, initial_u[signal], dx=dx[signal])
            resampled_u = np.hstack([resampled_u, resampled_u[-1]])
            nb_samples = len(resampled_u)
            extruded_distributions[signal][nb_times - i, :nb_samples] = resampled_u
            initial_u[signal] = final_u[signal]
        y_pos += dy
        initial_nb_nodes_per_cells, initial_x, initial_D = \
            final_nb_nodes_per_cells.copy(), final_x.copy(), final_D.copy()
    for line in lines:
        axis.add_line(line)
    for signal in signals:
        axis.imshow(extruded_distributions[signal], cmap=plt.get_cmap(colormaps[signal]),
                    interpolation=interpolation, filterrad=filterrad,
                    extent=[0, max_length, ylim[0], ylim[1]],
                    vmin=0, vmax=vmax[signal], aspect='auto', alpha=alpha)
    axis.grid(True)
    return figure, axis


#####################################
# Utilities for plotting diameters. #
#####################################

def init_diameter_plot(
        xlim,
        ylim,
        fontsize="x-large",
        figsize=None,
        figure=None,
        axis=None):
    """
    Create and return a Figure object and an Axes object ready for plotting the diameters.

    Parameters
    ----------
    xlim : tuple of two floats
        Limits of the x-axis: ax1.set_xlim(*xlim).
    ylim : tuple of two floats
        Limits of the y-axis: ax1.set_ylim(*ylim).
    figure : Figure instance, default: None
        If not None, figure is used for creating Axes, and is return as first element of
        the output tuple.
    axis : Axis instance, default: None
        If not None, axis is used for plotting and is returned as second element of the
        output tuple.

    See the plot_diameters function for definitions of other parameters.

    Outputs : figure, axis
    """
    if axis is None:
        if figure is None:
            figure, axis = plt.subplots(figsize=figsize)
        else:
            axis = figure.add_subplot(111)
    axis.set_xlim(*xlim)
    axis.set_ylim(*ylim)
    axis.grid(True)
    axis.set_xlabel("Cell index", size=fontsize)
    axis.set_ylabel(r"Cell diameter ($\mu$m)", size=fontsize)
    for label in axis.get_xticklabels() + axis.get_yticklabels():
        label.set_fontsize(fontsize)
    return figure, axis


def fill_diameter_plot(
        axis,
        cell_file,
        time,
        fixed_cambium_right=True,
        color="k",
        marker="o",
        linestyle="-",
        linewidth=1):
    """Clear axes and plot the diameters on it.

    Parameters
    ----------
    axis : Pyplot Axes instance
        Axis on which the diameters are plotted.

    See the plot_diameters function for definitions of other parameters.

    Output : axis
    """
    for line in axis.get_lines():
        line.remove()
    D = cell_file.diameters(time)
    nb_points = D.shape[0]
    if fixed_cambium_right:
        D = D[::-1]
    axis.plot(np.arange(1, nb_points + 1), D,
              color=color, marker=marker,
              linestyle=linestyle, linewidth=linewidth)
    return axis


def plot_diameters(
        cell_file,
        time=-1,
        date=False,
        fixed_cambium_right=True,
        color="k",
        marker="o",
        linestyle="-",
        linewidth=1,
        fontsize="x-large",
        xlim=None,
        ylim=None,
        figsize=None,
        figure=None,
        axis=None
        ):
    """Plot the diameter of the cells of a file.

    Parameters
    ----------
    cell_file : CellFile instance
        The cell file from which diameters are plotted.
    time : int, optional, default: -1
        Time in the growth process at which the diameters are plotted.
        Default is the last time step (-1).
    date : Boolean, optional, default: False
        If True, writes current date on the plot.
    color : a color, optional, default: 'k'
        The color in which the line is plotted.
    fontsize : int or str, optional, default: "x-large"
        Size argument for labels.
    xlim : tuple of two floats, optional, default: None
        Limits of the x-axis: ax.set_xlim(*xlim).
    ylim : tuple of two floats, optional, default: None
        Limits of the y-axis: ax.set_ylim(*xlim).
    figsize : tuple of two floats, optional, default: None
        Size of the figure (figsize argument in plt.subplot).
    fixed_cambium_right : Boolean, optional, default: True
        If True, the cambium is fixed on the right side of the plot.
    figure : Figure instance, default: None
        If not None, figure is used for creating Axes, and is return as first element of
        the output tuple.
    axis : Axis instance, default: None
        If not None, axis is used for plotting and is returned as second element of the
        output tuple.

    Outputs : figure, axis
    """
    if axis is None:
        if figure is None:
            figure, axis = plt.subplots(figsize=figsize)
        else:
            axis = figure.add_subplot(111)
    nb_cells = cell_file.number_of_cells(time)
    if xlim is None:
        xlim = (0, nb_cells + 1)
    if ylim is None:
        ylim = (0, cell_file.diameters(time).max() + 4)
    figure, axis = init_diameter_plot(
            xlim, ylim,
            fontsize=fontsize,
            figsize=figsize,
            figure=figure,
            axis=axis)
    axis = fill_diameter_plot(
            axis,
            cell_file,
            time,
            fixed_cambium_right=fixed_cambium_right,
            color=color, marker=marker, linestyle=linestyle, linewidth=linewidth)
    return figure, axis

# End of diameter plotting utilities.


###################################
# Utilities for plotting strains. #
###################################

def init_strain_plot(
        xlim,
        fontsize="x-large",
        figsize=None,
        figure=None):
    """
    Create and return a Figure object and an Axes object ready for plotting the strains.

    Parameters
    ----------
    xlim : tuple of two floats
        Limits of the x-axis: ax1.set_xlim(*xlim).
    ylim : tuple of two floats
        Limits of the y-axis: ax1.set_ylim(*ylim).
    figure : Figure instance, default: None
        If not None, figure is used for creating Axes, and is return as first element of
        the output tuple.

    See the plot_strains function for definitions of other parameters.

    Outputs : figure, axis
    """
    if figure is not None:
        axis = figure.add_subplot(111)
    else:
        figure, axis = plt.subplots(figsize=figsize)
    axis.set_xlim(*xlim)
    axis.grid(True)
    axis.set_xlabel("Cell index", size=fontsize)
    axis.set_ylabel(r"Strain rate ($s^-1$)", size=fontsize)
    for label in axis.get_xticklabels() + axis.get_yticklabels():
        label.set_fontsize(fontsize)
    return figure, axis


def fill_strain_plot(
        axis,
        cell_file,
        time,
        fixed_cambium_right=True,
        color="k",
        marker="o",
        linestyle="-",
        linewidth=1):
    """Clear axes and plot the strain rates on it.

    Parameters
    ----------
    axis : Pyplot Axes instance
        Axis on which the strain rates are plotted.

    See the plot_strains function for definitions of other parameters.

    Output : axis
    """
    for line in axis.get_lines():
        line.remove()
    S = cell_file.S_history[time]
    nb_points = S.shape[0]
    if fixed_cambium_right:
        S = S[::-1]
    axis.plot(np.arange(1, nb_points + 1), S,
              color=color, marker=marker,
              linestyle=linestyle, linewidth=linewidth)
    return axis


def plot_strains(
        cell_file,
        time=-1,
        date=False,
        fixed_cambium_right=True,
        color="k",
        marker="o",
        linestyle="-",
        linewidth=1,
        fontsize="x-large",
        xlim=None,
        figsize=None
        ):
    """Plot the strain rates of the cells of a file.

    Parameters
    ----------
    cell_file : CellFile instance
        The cell file from which the strain rates are plotted.
    time : int, optional, default: -1
        Time in the growth process at which the strain rates are plotted.
        Default is the last time step (-1).
    date : Boolean, optional, default: False
        If True, writes current date on the plot.
    fixed_cambium_right : Boolean, optional, default: True
        If True, the cambium is fixed on the right side of the plot.
    color : a color, optional, default: 'k'
        The color in which the line is plotted.
    fontsize : int or str, optional, default: "x-large"
        Size argument for any label.
    xlim : tuple of two floats, optional, default: None
        Limits of the x-axis: ax.set_xlim(*xlim).
    figsize : tuple of two floats, optional, default: None
        Size of the figure (figsize argument in plt.subplot).

    Output : Returns a Matplotlib Figure instance.
    """
    nb_cells = cell_file.number_of_cells(time)
    if xlim is None:
        xlim = (0, nb_cells + 1)
    fig, axis = init_strain_plot(
            xlim,
            fontsize=fontsize,
            figsize=figsize)
    axis = fill_strain_plot(
            axis,
            cell_file,
            time,
            fixed_cambium_right=fixed_cambium_right,
            color=color, marker=marker, linestyle=linestyle, linewidth=linewidth)
    return fig

# End of strain plotting utilities.


######################################################
# Utilities for plotting the total length and speed. #
######################################################

def plot_total_length(
        cell_file,
        time_data=None,
        length_data=None,
        color="k",
        linestyle="-",
        linewidth=1,
        fontsize="x-large",
        xlim=None,
        figsize=None,
        figure=None,
        axis=None
        ):
    """
    Plot the total length of a cell file over time.

    Parameters
    ----------
    cell_file : CellFile instance
        The cell file from which strain rates are plotted.
    time_data : 1-D array, optional, default: None
        Time values for the x axis.
    length_data : 1-D array, optional, default: None
        Length values for the y axis.
    color : a color, optional, default: 'k'
        The color in which the line is plotted.
    fontsize : int or str, optional, default: "x-large"
        Size argument for labels.
    xlim : tuple of two floats, optional, default: None
        Limits of the x-axis: ax.set_xlim(*xlim).
    figsize : tuple of two floats, optional, default: None
        Size of the figure (figsize argument in plt.subplot).
    figure : Figure instance, default: None
        If not None, figure is used for creating Axes, and is return as first element of
        the output tuple.
    axis : Axis instance, default: None
        If not None, axis is used for plotting and is returned as second element of the
        output tuple.

    Outputs : figure, axis
    """
    if axis is None:
        if figure is None:
            figure, axis = plt.subplots(figsize=figsize)
        else:
            axis = figure.add_subplot(111)
    if xlim is not None:
        axis.set_xlim(*xlim)
    axis.set_xlabel("Day of year", size=fontsize)
    axis.set_ylabel(r"Length ($\mu m$)", size=fontsize)
    axis.grid(True)
    if length_data is None:
        length_data = cell_file.total_growth_curve()
        length_data = length_data[:-1]
    nb_samples = len(length_data)
    if time_data is None:
        start_day = cell_file.start_time.days
        # if it was a datetime: timetuple().tm_yday
        stop_day = cell_file.stop_time.days
        time_data = np.linspace(start_day, stop_day, nb_samples)
    if len(time_data) != len(length_data):
        raise ValueError("Time data and length data lengths do not match.")
    axis.plot(time_data, length_data,
              color=color, linestyle=linestyle, linewidth=linewidth)
    for label in axis.get_xticklabels() + axis.get_yticklabels():
        label.set_fontsize(fontsize)
    return figure, axis


def plot_total_speed(
        cell_file,
        time_data=None,
        speed_data=None,
        color="k",
        linestyle="-",
        linewidth=1,
        fontsize="x-large",
        xlim=None,
        figsize=None,
        figure=None
        ):
    """
    Plot the total growth speed of a cell file over time.

    Parameters
    ----------
    cell_file : CellFile instance
        The cell file from which strain rates are plotted.
    time_data : 1-D array, optional, default: None
        Time values for the x axis.
    speed_data : 1-D array, optional, default: None
        Speed values for the y axis.
    color : a color, optional, default: 'k'
        The color in which the line is plotted.
    fontsize : int or str, optional, default: "x-large"
        Size argument for labels.
    xlim : tuple of two floats, optional, default: None
        Limits of the x-axis: ax.set_xlim(*xlim).
    figsize : tuple of two floats, optional, default: None
        Size of the figure (figsize argument in plt.subplot).
    figure : Figure instance, default: None
        If not None, figure is used for creating Axes, and is return as first element of
        the output tuple.

    Outputs : figure, axis
    """
    if figure is not None:
        axis = figure.add_subplot(111)
    else:
        figure, axis = plt.subplots(figsize=figsize)
    axis.set_xlabel("Day of year", size=fontsize)
    axis.set_ylabel(r"Speed ($\mu m.h^{-1}$)", size=fontsize)
    axis.grid(True)
    if speed_data is None:
        speed_data = cell_file.total_growth_speed_curve()
    nb_samples = len(speed_data)
    if time_data is None:
        start_day = cell_file.start_time.days
        stop_day = cell_file.stop_time.days
        time_data = np.linspace(start_day, stop_day, nb_samples)
    if len(time_data) != len(speed_data):
        raise ValueError("Time data and speed data lengths do not match.")
    axis.plot(time_data, speed_data,
              color=color, linestyle=linestyle, linewidth=linewidth)
    for label in axis.get_xticklabels() + axis.get_yticklabels():
        label.set_fontsize(fontsize)
    return figure, axis

# End of total length and speed plotting utilities.


#############################################################
# Utilities for plotting variables related to cell numbers. #
#############################################################

def nb_cells_curves(cell_file):
    """Return the number of cells in each phase over the time."""
    start_day = cell_file.start_time.days
    stop_day = cell_file.stop_time.days
    time = np.arange(start_day, stop_day + 1)[:-1]
    nb_samples = len(time)
    time_samples = [int(cell_file.conversion * (t - start_day)) for t in time]
    nb_dividing_cells = np.zeros(nb_samples)
    nb_enlarging_cells = np.zeros(nb_samples)
    nb_non_growing_cells = np.zeros(nb_samples)
    nb_cells = np.empty(nb_samples)
    for i, t in enumerate(time_samples):
        nb_dividing_cells[i], nb_enlarging_cells[i], nb_non_growing_cells[i], \
            nb_cells[i] = cell_file.number_of_cells_in_each_phase(t)
    return time, nb_dividing_cells, nb_enlarging_cells, \
        nb_non_growing_cells, nb_cells


def plot_cell_accumulation(
        cell_file,
        time_data=None,
        number_data=None,
        color="k",
        linestyle="-",
        linewidth=1,
        fontsize="x-large",
        xlim=None,
        figsize=None,
        figure=None,
        axis=None
        ):
    """
    Plot the total number of cells in the file over time.

    Parameters
    ----------
    cell_file : CellFile instance
        The cell file from which strain rates are plotted.
    time_data : 1-D array, optional, default: None
        Time values for the x axis.
    number_data : 1-D array, optional, default: None
        Cell number values for the y axis.
    color : a color, optional, default: 'k'
        The color in which the line is plotted.
    fontsize : int or str, optional, default: "x-large"
        Size argument for labels.
    xlim : tuple of two floats, optional, default: None
        Limits of the x-axis: ax.set_xlim(*xlim).
    figsize : tuple of two floats, optional, default: None
        Size of the figure (figsize argument in plt.subplot).
    figure : Figure instance, default: None
        If not None, figure is used for creating Axes, and is return as first element of
        the output tuple.
    axis : Axis instance, default: None
        If not None, axis is used for plotting and is returned as second element of the
        output tuple.

    Outputs : figure, ax1
    """
    if axis is None:
        if figure is None:
            figure, axis = plt.subplots(figsize=figsize)
        else:
            axis = figure.add_subplot(111)
    axis.set_xlabel("Day of year", size=fontsize)
    axis.set_ylabel(r"Number of cells", size=fontsize)
    axis.grid(True)
    time_computed = None
    if number_data is None:
        time_computed, _, _, _, number_data = nb_cells_curves(cell_file)
    if time_data is None:
        if time_computed is None:
            start_day = cell_file.start_time.days
            stop_day = cell_file.stop_time.days
            time_data = np.arange(start_day, stop_day + 1)[:-1]
        else:
            time_data = time_computed
    if len(time_data) != len(number_data):
        raise ValueError("Time data and number data lengths do not match.")
    axis.plot(time_data, number_data,
              color=color, linestyle=linestyle, linewidth=linewidth)
    for label in axis.get_xticklabels() + axis.get_yticklabels():
        label.set_fontsize(fontsize)
    return figure, axis


def plot_cell_numbers(
        cell_file,
        time_data=None,
        number_data=None,
        smoothing=True,
        window_size=31,
        order=1,
        colors=['green', 'b'],
        linestyle="-",
        linewidth=1,
        fontsize="x-large",
        xlim=None,
        figsize=None,
        legend=True,
        figure=None,
        axis=None
        ):
    """
    Plot the number of cells in each zone over time.

    Parameters
    ----------
    cell_file : CellFile instance
        The cell file from which strain rates are plotted.
    time_data : 1-D array, optional, default: None
        Time values for the x axis.
    number_data : tuple of three 1-D arrays, optional, default: None
        Cell number values for the y axis.
    smoothing : Boolean, default: False
        If True, the cell number values for the number_data parameter are smoothed using
        the Savitzky-Golay filter.
    window_size : int, default:31
        Smoothing parameter, see the savitzky_golay function docstring.
    order : int, default:1
        Smoothing parameter, see the savitzky_golay function docstring.
    colors : list of colors, optional, default: ['green', 'b']
        The color in which the line is plotted.
    fontsize : int or str, optional, default: "x-large"
        Size argument for labels.
    xlim : tuple of two floats, optional, default: None
        Limits of the x-axis: ax.set_xlim(*xlim).
    figsize : tuple of two floats, optional, default: None
        Size of the figure (figsize argument in plt.subplot).
    legend : Boolean, optional, default: True
        If True, a legend is displayed.
    figure : Figure instance, default: None
        If not None, figure is used for creating Axes, and is returned as first
        element of the output tuple.
    axis : Axis instance, default: None
        If not None, axis is used for plotting and is returned as second element of the
        output tuple.

    Outputs : figure, ax1
    """
    if axis is None:
        if figure is None:
            figure, axis = plt.subplots(figsize=figsize)
        else:
            axis = figure.add_subplot(111)
    axis.set_xlabel("Day of year", size=fontsize)
    axis.set_ylabel(r"Number of cells", size=fontsize)
    axis.grid(True)
    time_computed = None
    if number_data is None:
        nb_cells_curves_cumputed = nb_cells_curves(cell_file)
        time_computed = nb_cells_curves_cumputed[0]
        number_data = nb_cells_curves_cumputed[1:3]
    if time_data is None:
        if time_computed is None:
            start_day = cell_file.start_time.days
            stop_day = cell_file.stop_time.days
            time_data = np.arange(start_day, stop_day + 1)[:-1]
        else:
            time_data = time_computed
    for numbers in number_data:
        if len(time_data) != len(numbers):
            raise ValueError("Time data and number data lengths do not match.")
    if smoothing:
        smoothed_number_data = []
        for numbers in number_data:
            smoothed_numbers = savitzky_golay(numbers, window_size, order=order)
            smoothed_number_data.append(smoothed_numbers)
        number_data = smoothed_number_data
    axis.plot(time_data, number_data[0],
              color=colors[0], linestyle=linestyle,
              label="Cambial cells")
    axis.plot(time_data, number_data[1],
              color=colors[1], linestyle=linestyle,
              label="Enlarging cells")
    if legend:
        axis.legend(loc='best', fontsize=fontsize)
    for label in axis.get_xticklabels() + axis.get_yticklabels():
        label.set_fontsize(fontsize)
    return figure, axis
