#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Various classes for secondary windows in which a figure is embedded.
"""

from collections import deque
from itertools import islice

import numpy as np
from matplotlib.figure import Figure

from traits.api import HasTraits, Instance, Range, Enum, Float
from traitsui.api import Handler, View, Item

from .mplfigure_qt import MPLFigureEditor

# Utility functions for drawing and plotting outputs from simulations.
from .plotutils import (init_cell_file_drawing, fill_cell_file_drawing,
                        init_diameter_plot, fill_diameter_plot,
                        init_strain_plot, fill_strain_plot,
                        plot_total_length, plot_total_speed,
                        nb_cells_curves, plot_cell_accumulation,
                        plot_cell_numbers)

fontsize = "x-large"


class PlotWindowHandler(Handler):
    """Handler for SnapshotPlot and GrowthPlot objects."""
    def closed(self, info, is_ok):
        """Inform the parent that the window has been closed."""
        if info.object.parent is not None:
            if info.object.__class__ == SnapshotPlot:
                info.object.parent.snapshot_plot = None
            elif info.object.__class__ == GrowthPlot:
                info.object.parent.growth_plot = None
            elif info.object.__class__ == DynamicsPlot:
                info.object.parent.dynamics_plot = None
            elif info.object.__class__ == FunctionOfTimePlot:
                info.object.parent.function_of_time_plot = None


def moving_window(iterable, size):
    """Moving window on an iterable.

    From
    http://sametmax.com/implementer-une-fenetre-glissante-en-python-avec-un-deque/

    """
    iterable = iter(iterable)
    d = deque(islice(iterable, size), size)
    yield d
    for x in iterable:
        d.append(x)
        yield d


def moving_average(data, window):
    """Return the moving average of data."""
    N = len(data)
    averaged_data = np.empty(N)
    if N < window:
        for i in range(N//2):
            averaged_data[i] = sum(data[:2 * i + 1]) / (2 * i + 1)
        for i in range(N//2, N):
            averaged_data[i] = sum(data[i - (N - 1 - i):]) / (2 * N - 2 * i - 1)
        return averaged_data
    start = window // 2
    stop = N - start
    for i in range(start):
        averaged_data[i] = sum(data[:2 * i + 1]) / (2 * i + 1)
    for i, data_slice in enumerate(moving_window(data, size=window), start):
        averaged_data[i] = sum(data_slice) / window
    for i in range(stop, N):
        averaged_data[i] = sum(data[i - (N - 1 - i):]) / (2 * N - 2 * i - 1)
    return averaged_data


class SnapshotPlot(HasTraits):
    """A SnapshotPlot object is a window displaying an output at a given
    simulation time. Its view is composed of
    * a Matplotlib figure (as instance of MPLFigure);
    * a ruler for changing the snapshot time (as a Range trait);
    * a drop down menu for selecting the output to display (as an Enum trait).

    The output to display can be
    * the spatial representation of the cell file, with signal concentrations
      and optionally thresholds and strain rates;
    * the cell diameters, plotted against cell indices;
    * the strain rates, plotted against cell indices.

    """
    figure = Instance(Figure, ())

    display = Enum("cell file", "diameters", "strain rates")

    start = Float(0.)
    stop = Float(20.)

    snapshot_time = Range('start', 'stop', 'stop', label="Snapshot time")

    traits_view = View(
        Item('figure',
             editor=MPLFigureEditor(),
             springy=True,
             show_label=False),
        Item('snapshot_time', show_label=False),
        Item('display', show_label=False),
        resizable=True
        )

    def __init__(self, cell_file, display, start, stop,
                 fixed_cambium_right=False):
        self.cell_file = cell_file
        self.display = display
        self.start = start
        self.stop = stop
        self.time = int(self.cell_file.conversion *
                        (self.snapshot_time - self.start))
        self.cell_file_xlim = (0, 1.01 * self.cell_file.length(self.time))
        self.u_max = max([max([max(self.cell_file.u_history[t][i])
                             for i in range(len(self.cell_file.u_history[t]))])
                          for t in range(self.time + 1)])
        self.S_max = max([max(self.cell_file.S_history[t])
                          for t in range(self.time + 1)])
        if self.S_max == 0:
            self.S_max = 1
        self.nb_cells = self.cell_file.number_of_cells(self.time)
        self.diameters_ylim = (0,
                               self.cell_file.diameters(self.time).max() + 4)
        self.threshold_list = []
        self.show_strain_rates = False
        self.colored_zones = False
        self.fixed_cambium_right = fixed_cambium_right
        self.on_trait_change(self._on_snapshot_time_changed, 'snapshot_time')
        self.on_trait_change(self._on_display_changed, 'display')
        self.init_plot()
        self.update_plot()

    def _on_snapshot_time_changed(self):
        """Callback of the snapshot_time trait (ruler).

        Updates the figure only if the snapshot time has been changed by more
        than one day.
        """
        if self.figure.canvas is None:
            return
        time = int(self.cell_file.conversion *
                   (self.snapshot_time - self.start))
        if time != self.time:
            self.time = time
            self.update_plot()

    def _on_display_changed(self):
        """Callback of the display trait (drop down menu)."""
        self.figure.clear()
        self.init_plot()
        self.update_plot()

    def init_plot(self, fontsize=fontsize):
        """Set up the axes, with limits and labels."""
        if self.display == "cell file":
            ylim = (0, 1.01 * self.u_max)
            if ylim[1] < 2.2 * self.cell_file.concentration_scale():
                ylim = (0, 2.2 * self.cell_file.concentration_scale())
            # prepare figure and axes
            self.ax1 = init_cell_file_drawing(
                self.cell_file_xlim, ylim,
                fixed_cambium_right=self.fixed_cambium_right,
                fontsize=fontsize,
                figure=self.figure
                )[1]
        elif self.display == "diameters":
            xlim = (0, self.nb_cells + 1)
            # prepare figure and axes
            self.ax1 = init_diameter_plot(
                xlim, self.diameters_ylim,
                fontsize=fontsize,
                figure=self.figure
                )[1]
        elif self.display == "strain rates":
            xlim = (0, self.nb_cells + 1)
            # prepare figure and axes
            self.ax1 = init_strain_plot(
                xlim,
                fontsize=fontsize,
                figure=self.figure
                )[1]

    def update_plot(self):
        """Do the actual plotting work."""
        if self.display == "cell file":
            if self.show_strain_rates:
                S = self.cell_file.S_history[self.time] / self.S_max
            else:
                S = None
            # plot cell file
            self.ax1 = fill_cell_file_drawing(
                self.cell_file,
                self.ax1,
                self.time,
                self.cell_file_xlim,
                threshold_list=self.threshold_list,
                strain_rates=S,
                colored_zones=self.colored_zones
                )
        elif self.display == "diameters":
            self.ax1 = fill_diameter_plot(
                self.ax1,
                self.cell_file,
                self.time,
                fixed_cambium_right=self.fixed_cambium_right
                )
        elif self.display == "strain rates":
            self.ax1 = fill_strain_plot(
                self.ax1,
                self.cell_file,
                self.time,
                fixed_cambium_right=self.fixed_cambium_right
                )
        canvas = self.figure.canvas
        if canvas is not None:
            canvas.draw()

# end of SnapshotPlot class


class GrowthPlot(HasTraits):
    """A GrowthPlot object is a window plotting the time-course of a variable
    related to the growth. Its view is composed of
    * a Matplotlib figure (as instance of MPLFigure);
    * a drop down menu for selecting the variable to plot (as an Enum trait).

    The variable to plot can be
    * the total length of the cell file;
    * the speed of growth of the total length.

    """
    figure = Instance(Figure, ())

    display = Enum("length", "speed")

    traits_view = View(
        Item('figure',
             editor=MPLFigureEditor(),
             springy=True,
             show_label=False),
        Item('display', show_label=False),
        resizable=True
        )

    def __init__(self, cell_file, display):
        self.cell_file = cell_file
        self.display = display
        # length values
        self.length = self.cell_file.total_growth_curve()
        self.length = self.length[:-1]
        # speed values
        self.speed = self.cell_file.total_growth_speed_curve()
        # time values for the x axis
        nb_samples = len(self.speed)
        start_day = cell_file.start_time.days
        stop_day = cell_file.stop_time.days
        self.time = np.linspace(start_day, stop_day, nb_samples)
        # create axis
        self.ax1 = self.figure.add_subplot(111)
        # plot
        self.plot()
        self.on_trait_change(self._on_display_changed, 'display')

    def _on_display_changed(self):
        """Callback of the display trait (drop down menu)."""
        self.figure.clear()
        self.plot()

    def plot(self):
        for line in self.ax1.get_lines():
            line.remove()
        if self.display == "length":
            self.ax1 = plot_total_length(self.cell_file,
                                         time_data=self.time,
                                         length_data=self.length,
                                         figure=self.figure)[1]
        elif self.display == "speed":
            self.ax1 = plot_total_speed(self.cell_file,
                                        time_data=self.time,
                                        speed_data=self.speed,
                                        figure=self.figure)[1]
        canvas = self.figure.canvas
        if canvas is not None:
            canvas.draw()

# end of GrowthPlot class


class DynamicsPlot(HasTraits):
    """A DynamicsPlot object is a window plotting the time-course of variables
    related to cell numbers. Its view is composed of
    * a Matplotlib figure (as instance of MPLFigure);
    * a drop down menu for selecting the variable to plot (as an Enum trait).

    The variables to plot can be
    * the accumulation of cells produced;
    * the number of cells in each phase;
    * the entry rate of cells in each phase.

    """
    figure = Instance(Figure, ())

    display = Enum(
        "Accumulation of cells produced",
        "Number of cells in each phase",
        "Cell entry rate in each phase")

    traits_view = View(
        Item('figure',
             editor=MPLFigureEditor(),
             springy=True,
             show_label=False),
        Item('display', show_label=False),
        resizable=True
        )

    def __init__(self, cell_file, display, start, stop):
        self.cell_file = cell_file
        self.display = display
        self.time, self.nb_dividing_cells, self.nb_enlarging_cells, \
            self.nb_non_growing_cells, self.nb_cells \
            = nb_cells_curves(self.cell_file)
        self.cell_numbers = (self.nb_dividing_cells, self.nb_enlarging_cells,
                             self.nb_non_growing_cells)
        start_day = cell_file.start_time.days
        time_samples = [int(cell_file.conversion * (t - start_day))
                        for t in self.time]
        self.rate_dividing, self.rate_enlarging, self.rate_non_growing \
                = None, None, None
        self.init_plot()
        self.update_plot()
        self.on_trait_change(self._on_display_changed, 'display')

    def compute_entry_rates(self, T_samples, window=7):
        nb_samples = len(T_samples)
        rate_dividing = np.zeros(nb_samples)
        rate_enlarging = np.zeros(nb_samples)
        rate_non_growing = np.zeros(nb_samples)
        for i, t in enumerate(T_samples[1:], 1):
            t_prev = T_samples[i - 1]
            new_dividing, new_enlarging, new_non_growing = 0, 0, 0
            for cell in self.cell_file.cells:
                identity = cell.get_identity(t)
                prev_identity = cell.get_identity(t_prev)
                if identity == "dividing" and \
                        (prev_identity is None or prev_identity == "inactive"):
                    new_dividing += 1
                elif identity == "enlarging" and prev_identity == "dividing":
                    new_enlarging += 1
                elif identity == "non growing" \
                        and prev_identity != "non growing":
                    new_non_growing += 1
            rate_dividing[i] = new_dividing
            rate_enlarging[i] = new_enlarging
            rate_non_growing[i] = new_non_growing
        self.rate_dividing = moving_average(rate_dividing, window)
        self.rate_enlarging = moving_average(rate_enlarging, window)
        self.rate_non_growing = moving_average(rate_non_growing, window)

    def _on_display_changed(self):
        """Callback of the display trait (drop down menu)."""
        self.figure.clear()
        self.init_plot()
        self.update_plot()

    def init_plot(self):
        self.ax1 = self.figure.add_subplot(111)

    def update_plot(self):
        for line in self.ax1.get_lines():
            line.remove()
        if self.display == "Accumulation of cells produced":
            self.ax1 = plot_cell_accumulation(self.cell_file,
                                              time_data=self.time,
                                              number_data=self.nb_cells,
                                              axis=self.ax1)[1]
        if self.display == "Number of cells in each phase":
            self.ax1 = plot_cell_numbers(self.cell_file,
                                         time_data=self.time,
                                         number_data=self.cell_numbers,
                                         axis=self.ax1)[1]
        if self.display == "Cell entry rate in each phase":
            if self.rate_dividing is None:
                self.compute_entry_rates(time_samples, window=7)
            self.ax1.plot(self.time, self.rate_dividing,
                          color='green', linestyle='-', label="Cambial cells")
            self.ax1.plot(self.time, self.rate_enlarging,
                          color='b', linestyle='-', label="Enlarging cells")
            self.ax1.set_ylabel(r"Cell entry rate (day¯¹)", size=fontsize)
            self.ax1.legend(loc='best')
        canvas = self.figure.canvas
        if canvas is not None:
            canvas.draw()

# end of DynamicsPlot class


class CellProcessPlot(HasTraits):
    """A CellProcessPlot object is a window plotting some characteristics of
    the cell processes, for each cell along the file.

    Its view is composed of
    * a Matplotlib figure (as instance of MPLFigure);
    * a drop down menu for selecting the variable to plot (as an Enum trait).

    The variables to plot can be
    * the time spent in each phase;
    * the mean rate in each phase.

    """
    figure = Instance(Figure, ())

    display = Enum(
        "Cell residence time in each phase",
        "Mean rate in each phase")

    traits_view = View(
        Item('figure',
             editor=MPLFigureEditor(),
             springy=True,
             show_label=False),
        Item('display', show_label=False),
        resizable=True
        )

    def __init__(self, cell_file, display, fixed_cambium_right=False):
        self.cell_file = cell_file
        self.display = display
        self.fixed_cambium_right = fixed_cambium_right
        if fixed_cambium_right:
            self.cells = self.cell_file.cells[::-1]
        else:
            self.cells = self.cell_file.cells
        self.nb_cells = len(self.cells)
        self.X = range(1, self.nb_cells + 1)
        self.time_dividing = np.zeros(self.nb_cells)
        self.time_enlarging = np.zeros(self.nb_cells)
        self.time_non_growing = np.zeros(self.nb_cells)
        self.compute_transition_times()
        self.compute_residence_times()
        self.compute_mean_rates()
        self.init_plot()
        self.update_plot()
        self.on_trait_change(self._on_display_changed, 'display')

    def compute_transition_times(self):
        """Compute the time of each transition between phases."""
        self.transition_times = []
        for i, cell in enumerate(self.cells):
            if "enlarging" in cell.identities:
                start_enlarging_time = cell.identities.index("enlarging") \
                                       / self.cell_file.conversion
            else:
                start_enlarging_time = None
            if "non growing" in cell.identities:
                stop_enlarging_time = cell.identities.index("non growing") \
                                      / self.cell_file.conversion
            else:
                stop_enlarging_time = None
            self.transition_times.append((start_enlarging_time,
                                          stop_enlarging_time))

    def compute_residence_times(self):
        """Compute the residence time of each cell in each phase."""
        self.division_residence_times = np.empty(self.nb_cells)
        self.enlargement_residence_times = np.empty(self.nb_cells)
        for i, (start_enlarging_time, stop_enlarging_time) \
                in enumerate(self.transition_times):
            T_max = (len(self.cells[i].identities) - 1) \
                    / self.cell_file.conversion
            if start_enlarging_time is not None:
                division_residence_time = start_enlarging_time
            elif stop_enlarging_time is not None:
                division_residence_time = stop_enlarging_time
            else:
                division_residence_time = T_max
            if start_enlarging_time is None:
                enlargement_residence_time = 0
            elif stop_enlarging_time is not None:
                enlargement_residence_time = stop_enlarging_time \
                                             - start_enlarging_time
            else:
                enlargement_residence_time = T_max - start_enlarging_time
            self.division_residence_times[i] = division_residence_time
            self.enlargement_residence_times[i] = enlargement_residence_time

    def compute_mean_rates(self):
        """Compute the mean rate of each process for each cell.

        In Henri Cuny's view, the mean enlargement rate of a cell is computed
        by dividing the total increase in cell size by the the residence time
        in the enlargement phase:
        r = size increase / residence time in enlargement phase

        With size increase = D_final - D_initial.
        D_initial = D_cambial ≅ 6µm
        """
        self.division_rates = np.empty(self.nb_cells)
        self.enlargement_rates = np.empty(self.nb_cells)
        D_0 = self.cell_file.initial_CRD
        for i, (division_residence_time, enlargement_residence_time) \
                in enumerate(zip(self.division_residence_times,
                                 self.enlargement_residence_times)):
            if enlargement_residence_time > 0:
                D_max = self.cells[i].diameters[-1]
                rate = (D_max - D_0) / enlargement_residence_time
            else:
                rate = 0
            self.enlargement_rates[i] = rate

    def _on_display_changed(self):
        """Callback of the display trait (drop down menu)."""
        self.figure.clear()
        self.init_plot()
        self.update_plot()

    def init_plot(self):
        self.ax1 = self.figure.add_subplot(111)
        self.ax1.set_xlabel("Cell index", size=fontsize)
        self.ax1.grid(True)

    def update_plot(self):
        for line in self.ax1.get_lines():
            line.remove()
        if self.display == "Cell residence time in each phase":
#            self.ax1.plot(self.X, self.division_residence_times,
#                           color = 'green', linestyle = '-',
#                           label = "Division")
            self.ax1.plot(self.X, self.enlargement_residence_times, 'ko-')
            self.ax1.set_ylabel(r"Residence time (days)", size=fontsize)
        elif self.display == "Mean rate in each phase":
            self.ax1.plot(self.X, self.enlargement_rates, 'ko-')
            self.ax1.set_ylabel(r"Rate (µm/day)", size=fontsize)
        canvas = self.figure.canvas
        if canvas is not None:
            canvas.draw()

# end of CellProcessPlot class


class FunctionOfTimePlot(HasTraits):
    """
    A FunctionOfTimePlot object is a window plotting a given function of time.

    Time unit is day.

    The view of an instance is composed of
    * a Matplotlib figure (as instance of MPLFigure);
    * a text box for choosing the time range of the plot (as a Float trait).

    """
    figure = Instance(Figure, ())

    time_range = Float(365)

    traits_view = View(
        Item('figure',
             editor=MPLFigureEditor(),
             springy=True,
             show_label=False),
        Item('time_range', show_label=False),
        resizable=True,
        handler=PlotWindowHandler()
        )

    def __init__(self, parent=None, function=None):
        self.parent = parent
        self.function = function
        self.init_plot()
        self.update_plot()
        self.on_trait_change(self._on_time_range_changed, 'time_range')

    def _on_time_range_changed(self):
        """Callback of the time_range trait."""
        self.figure.clear()
        self.init_plot()
        self.update_plot()

    def init_plot(self):
        self.ax1 = self.figure.add_subplot(111)
        self.ax1.set_xlabel("Time (days)", size=fontsize)
        self.ax1.grid(True)

    def update_plot(self):
        for line in self.ax1.get_lines():
            line.remove()
        X = np.linspace(0, self.time_range, 366)
        values = self.function.f(X)
        self.ax1.plot(X, values, 'b-')
        canvas = self.figure.canvas
        if canvas is not None:
            canvas.draw()

# end of BoundaryConditionPlot class
