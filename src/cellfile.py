#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
CellFile class.
"""

from itertools import chain
from collections import OrderedDict
from datetime import datetime, timedelta
from copy import copy
import numpy as np
import matplotlib.pyplot as plt

from .cell import Cell


class CellFile(object):
    """Radial cell file class.

    A CellFile object simulates a radial cell file produced by cambial activity.

    Acronyms:
    - CRD(s): cell radial diameter(s)
    - CWT(s): cell wall thickness(es)
    """
    time_units = {
        "second": 1,
        "minute": 60,
        "hour": 60 * 60,
        "day": 24 * 60 * 60}

    def __init__(
        self,
        nb_cells=4,
        initial_CRD=3.,
        initial_CWT=0.1,
        division_threshold=2,
        enlargement_threshold=1,
        growth_prefactor=.03,
        size_dependent_strains=False,
        data_time_unit=60*60*24,
        date=None,
    ):
        """
        Parameters
        ----------
        nb_cells : int
            initial number of cells in the file
        initial_CRD : float
            initial radial diameter of the cells
        initial_CWT : float
            initial wall thickness of the cells
        division_threshold : float
            concentration threshold above which a cell can be in division
        enlargement_threshold : float
            concentration threshold above which a cell is enlarging if not in
            division
        growth_prefactor : float
            factor for converting signal concentrations into a growth rate
        size_dependent_strains : Boolean (default: False)
            If True, strains are inversely proportional to cell diameters.
        data_time_unit : int
            time unit of data, in seconds, including signals (but not
            diffusion constant and time steps)
        date : datetime object
            date of initialization of the file
        """
        self.data_time_unit = data_time_unit

        if date is None:
            self.date = datetime(2010, 5, 8)
        else:
            self.date = date
        self.date_list = [self.date]

        # initial cell sizes
        self.initial_CRD = initial_CRD
        self.initial_CWT = initial_CWT

        self.cells = []
        for index in range(nb_cells):
            self.cells.append(Cell(index, 0, self.initial_CRD))
        self.nb_cells = nb_cells

        self.D_threshold = 2 * self.initial_CRD
        self.D_inv_threshold = 1 / self.D_threshold

        # threshold for cell division
        self.division_threshold = division_threshold

        # threshold for cell enlargement end
        self.enlargement_threshold = enlargement_threshold

        self.growth_prefactor = growth_prefactor / self.data_time_unit

        self.size_dependent_strains = size_dependent_strains

        # how the cell zones are defined
        self.identity_definition = "anatomy"

    def concentration_scale(self):
        """Return a 'characterstic concentration' uswful for plotting.

        Typically, it is the concentration threshold.
        """
        return self.division_threshold

    def init_history(self):
        """Initialize lists for keeping track of the cell file history."""
        self.dX_cell_history = []
        self.nodes_history = []
        self.u_history = []
        self.PIN_history = []
        self.R_history = []
        self.S_history = []
        self.cambial_history = []
        self.non_growing_history = []
        self.u_tot_history = []
        self.cum_u_evol_history = []

    def make_history(self, u, PIN, check_conservation=False):
        """Backup some current characterstics of the cell file."""
        self.nodes_history.append(self.nodes_per_cell.copy())
        self.dX_cell_history.append(self.dX_cell.copy())
        self.R_history.append(self.R.copy())
        self.cambial_history.append(copy(self.cambial_cells))
        self.non_growing_history.append(copy(self.non_growing_cells))
        self.S_history.append(self.S.copy())
        u_copy = [u_i.copy() for u_i in u]
        self.u_history.append(u_copy)
        if not all(elt is None for elt in PIN):
            PIN_copy = [copy(PIN_i) for PIN_i in PIN]
            self.PIN_history.append(PIN_copy)
        if check_conservation:
            self.u_tot_history.append(self.u_tot)
            self.cum_u_evol_history.append(self.cum_u_evol)

    def get_max_concentration(self, signal):
        """Return the maximum concentration of the signal over the whole history."""
        try:
            return max([self.u_history[t][signal] for t in range(len(self.u_history))])
        except AttributeError:
            raise AttributeError("The history has not been initialized.")

    def get_gradient_zones(self, cell_conc):
        """
        Return cell zones as defined by the gradient of identity signal and the
        thresholds.

        Returns
        -------
        zone_1 : list of ints
            Lists of the indices of the cells above the division threshold.
        zone_2 : list of ints
            Lists of the indices of the cells between the division threshold and the
            enlargement threshold.
        zone_3 : list of ints
            Lists of the indices of the cells below the division threshold.
        """
        div_id_conc = cell_conc[self.div_id_index]
        enl_id_conc = cell_conc[self.enl_id_index]
        zone_1 = np.where(div_id_conc >= self.division_threshold)[0]
        zone_2 = np.where((enl_id_conc >= self.enlargement_threshold)
                          & (div_id_conc < self.division_threshold))[0]
        zone_3 = np.where(enl_id_conc < self.enlargement_threshold)[0]
        zone_1.sort()
        zone_2.sort()
        zone_3.sort()
        return zone_1, zone_2, zone_3

    def determine_cell_identities(self, inactive=False):
        """Determine cell identities based on growth and anatomy."""
        if inactive:
            inactive_cells = np.arange(self.nb_cells)
            growing_cells, non_growing_cells, cambial_cells, dividing_cells = \
                    np.arange(0), np.arange(0), np.arange(0), np.arange(0)
        else:
            inactive_cells = np.arange(0)
            div_id_conc = self.cell_conc[self.div_id_index]
            enl_id_conc = self.cell_conc[self.enl_id_index]
            growing_cells = np.where((enl_id_conc >= self.enlargement_threshold)
                                     | (div_id_conc >= self.division_threshold))[0]
            non_growing_cells = np.array([i for i in range(self.nb_cells)
                                          if i not in growing_cells], dtype=int)
            cambial_cells = np.array([i for i in growing_cells
                                      if self.D_real[i] < self.D_threshold],
                                     dtype=int)
            dividing_cells = np.array([
                i for i in cambial_cells
                if self.cell_conc[self.div_id_index, i] >= self.division_threshold
            ], dtype=int)
        return inactive_cells, growing_cells, non_growing_cells, cambial_cells, \
                dividing_cells

    def get_cells_identities(self):
        """Return the current identities of the cells."""
        identities = []
        if self.identity_definition == "anatomy":
            # First step: catagorization based on anatomy and growth
            inactive, cambial, enlarging, non_growing = [], [], [], []
            for i in range(self.nb_cells):
                if i in self.inactive_cells:
                    inactive.append(i)
                elif i in self.cambial_cells:
                    cambial.append(i)
                else:
                    if i in self.non_growing_cells:
                        non_growing.append(i)
                    else:
                        enlarging.append(i)
            # Second step: cambial cells located after an enlarging cell are
            # recategorized as enlarging cells.
            if enlarging:
                enlarging_min = min(enlarging)
                cambial_copy = copy(cambial)
                for index in cambial_copy:
                    if index > enlarging_min:
                        cambial.remove(index)
                        enlarging.append(index)
            # Third step: now, the identities can be put in a list
            for i in range(self.nb_cells):
                if i in inactive:
                    identities.append("inactive")
                elif i in cambial:
                    identities.append("cambial")
                elif i in enlarging:
                    identities.append("enlarging")
                elif i in non_growing:
                    identities.append("non growing")
        elif self.identity_definition == "gradient":
            zone_1, zone_2, zone_3 = self.get_gradient_zones(self.cell_conc)
            for i in range(self.nb_cells):
                if i in zone_1:
                    identities.append("cambial")
                elif i in zone_2:
                    identities.append("enlarging")
                elif i in zone_3:
                    identities.append("non growing")
        else:
            raise ValueError("{} is not a valid cell identity definition".format(
                    self.identity_definition))
        if len(identities) != self.nb_cells:
            raise UserWarning("Identities are incomplete!")
        return identities

    def get_cell_zones_anatomy(self, time):
        """Return cell zones as defined by the anatomical criteria.

        Returns
        -------
        zone_1 : 1-D ndarray of ints
            Lists of the indices of the cells in the division zone.
        zone_2 : 1-D ndarray of ints
            Lists of the indices of the cells in the enlargement zone.
        zone_3 : 1-D ndarray of ints
            Lists of the indices of the cells in the non growing zone.
        """
        zone_1, zone_2, zone_3 = [], [], []
        for cell in self.cells:
            if time in cell.dates:
                i = cell.dates.index(time)
                identity = cell.identities[i]
                if identity == "cambial" or identity == "inactive":
                    zone_1.append(cell.indices[i])
                elif identity == "enlarging":
                    zone_2.append(cell.indices[i])
                elif identity == "non growing":
                    zone_3.append(cell.indices[i])
        return zone_1, zone_2, zone_3

    def get_cell_concentrations(self, u, concentration_operator):
        """Return signal concentrations of the cells."""
        conc = []
        for i, mechanism in enumerate(self.transport_mechanisms):
            if mechanism == "uniform diffusion":
                conc_i = np.dot(concentration_operator, u[i])
            else:
                conc_i = u[i]
            conc.append(conc_i)
        return np.array(conc)

    def cell_division(self, cell_index, u, PIN, nodes_per_cell=None):
        """Cell undergoes the division process (mitosis).

        A cell divides into two cells, whose characterstics are those of the initial
        cells of the file.
        """
        i = cell_index
        # create new cell
        new_index = i + 1
        new_cell = Cell()
        self.cells.insert(new_index, new_cell)
        # update total cell number
        self.nb_cells += 1
        # mesh refinement
        # u and PINs are refined accordingly
        cum_indices = nodes_per_cell.cumsum()
        cum_indices = np.insert(cum_indices, 0, 0)
        start, stop = cum_indices[i], cum_indices[i + 1]
        nb_nodes = stop - start
        for n, mechanism in enumerate(self.transport_mechanisms):
            un = u[n]
            PINn = PIN[n]
            if mechanism == "uniform diffusion":
                un = u[n]
                u_i = un[start:stop]
                u_i_new = np.empty(2*nb_nodes)
                if nb_nodes > 2:
                    divisor = u_i[:-2].copy()
                    divisor[divisor == 0.] = np.inf
                    r = (u_i[2:] / divisor)**(1/4)
                    r[r == 0.] = 1.
                    if i == 0:
                        r_left = 1
                    else:
                        r_left = (u_i[2] / u_i[1])**(1/2)
                    if i == self.nb_cells - 1:
                        r_right = 1
                    else:
                        r_right = (u_i[-1] / u_i[-2])**(1/2)
                    u_i_new[0] = 2 * u_i[0] / (1 + r_left)
                    u_i_new[1] = r_left * u_i_new[0]
                    u_i_new[2:-2:2] = 2 * u_i[1:-1]/(1 + r)
                    u_i_new[3:-2:2] = r * u_i_new[2:-2:2]
                    u_i_new[-2] = 2 * u_i[-1] / (1 + r_right)
                    u_i_new[-1] = r_right * u_i_new[-2]
                elif nb_nodes == 2:
                    delta = (u_i[1] - u_i[0])/2
                    u_i_new[::2] = u_i - delta
                    u_i_new[1::2] = u_i + delta
                elif nb_nodes == 1:
                    u_i_new[0], u_i_new[1] = u_i, u_i
                un = np.hstack((un[:start], u_i_new, un[stop:]))
            else:
                un = np.insert(un, i, un[i])
            if PINn is not None:
                PINn = np.insert(PINn, i, PINn[i])
            u[n] = un
            PIN[n] = PINn
        nodes_per_cell = np.insert(nodes_per_cell, i, nodes_per_cell[i])
        self.dX_cell[i] /= 2
        self.dX_cell = np.insert(self.dX_cell, i+1, self.dX_cell[i])
        self.D = self.dX_cell * nodes_per_cell
        self.int_S = np.insert(self.int_S, i+1, self.int_S[i])
        self.R = np.exp(self.int_S)
        self.D_real = self.R * self.D
        conc_operator = self.compute_concentration_operator(nodes_per_cell)
        self.cell_conc = self.get_cell_concentrations(u, conc_operator)
        self.S = np.zeros(self.nb_cells)
        self.compute_cell_strains()
        return u, PIN, nodes_per_cell, conc_operator

    def double_mesh_nodes(self, u, cell_index, nodes_per_cell):
        """Double number of mesh nodes in a cell, and update u accordingly."""
        i = cell_index
        cum_indices = nodes_per_cell.cumsum()
        cum_indices = np.insert(cum_indices, 0, 0)
        start, stop = cum_indices[i], cum_indices[i + 1]
        nb_nodes = stop - start
        for n, mechanism in enumerate(self.transport_mechanisms):
            if mechanism == "uniform diffusion":
                un = u[n]
                u_i = un[start:stop]
                u_i_new = np.empty(2*nb_nodes)
                if nb_nodes == 1:
                    u_i_new[::] = u_i[0]
                elif nb_nodes == 2:
                    delta = (u_i[1] - u_i[0])/2
                    u_i_new[::2] = u_i - delta
                    u_i_new[1::2] = u_i + delta
                elif nb_nodes > 2:
                    divisor = u_i[:-2].copy()
                    divisor[divisor == 0.] = np.inf
                    r = (u_i[2:] / divisor)**(1/4)
                    r[r == 0.] = 1.
                    if i == 0:
                        r_left = 1
                    else:
                        r_left = (u_i[2] / u_i[1])**(1/2)
                    if i == self.nb_cells - 1:
                        r_right = 1
                    else:
                        r_right = (u_i[-1] / u_i[-2])**(1/2)
                    u_i_new[0] = 2 * u_i[0] / (1 + r_left)
                    u_i_new[1] = r_left * u_i_new[0]
                    u_i_new[2:-2:2] = 2 * u_i[1:-1]/(1 + r)
                    u_i_new[3:-2:2] = r * u_i_new[2:-2:2]
                    u_i_new[-2] = 2 * u_i[-1] / (1 + r_right)
                    u_i_new[-1] = r_right * u_i_new[-2]
                un = np.hstack((un[:start], u_i_new, un[stop:]))
            else:
                un = u[n]
            u[n] = un
        nodes_per_cell[i] *= 2
        self.dX_cell[i] /= 2
        conc_operator = self.compute_concentration_operator(nodes_per_cell)
        return u, nodes_per_cell, conc_operator

    def check_conservation(self, t, i=0):
        """Check signal conservation.

        Works only for imposed flux.
        """
        # total amount of signal in the domain at time t
        self.u_tot = (self.cell_conc[0] / self.D_real_inv).sum()
        # time evolution of this quantity between t and t+1
        u_evolution = self.imposed_flux[t, i] - self.dt * self.alpha[i] * self.u_tot
        # update the cumulated time evolution
        self.cum_u_evol += u_evolution

    def compute_cell_strains(self):
        """If strain control is the same for all cells."""
        control_conc = self.cell_conc[self.control_indices]
        control_conc_in_growing_cells = np.take(
                control_conc, self.growing_cells, axis=1)
        conc_product = np.prod(control_conc_in_growing_cells, axis=0)
        D_growing_cells = self.D_real[self.growing_cells]
        self.S = np.zeros(self.nb_cells)
        if self.size_dependent_strains:
            self.S[self.growing_cells] = self.growth_prefactor * conc_product \
                    * self.initial_CRD / D_growing_cells
        else:
            self.S[self.growing_cells] = self.growth_prefactor * conc_product

    def grow_by_one_step_and_check_for_division(self, u, PIN):
        """
        Elementary growth increment, followed by cell identities update, and
        divisions if needed.
        """
        self.int_S += self.S * self.growth_time_step
        self.R = np.exp(self.int_S)
        self.D_real = self.R * self.D
        self.cell_conc = self.get_cell_concentrations(u, self.conc_operator)
        self.compute_cell_strains()
        # cambial_cells is iterated over in reverse order to avoid an index shift
        # problem if two cells or more divide within the same timestep
        division = False
        for i in self.cambial_cells[::-1]:
            if self.D_real[i] > self.D_threshold:
                if self.cell_conc[self.div_id_index, i] > self.division_threshold:
                    u, PIN, self.nodes_per_cell, self.conc_operator = \
                            self.cell_division(i, u, PIN, self.nodes_per_cell)
                    self.nb_nodes = self.nodes_per_cell.sum()
                    division = True
        for i, nb_nodes in enumerate(self.nodes_per_cell):
            if self.D_real[i] / nb_nodes > \
                    2 * self.initial_CRD / self.initial_nodes_per_cell:
                u, self.nodes_per_cell, self.conc_operator = \
                        self.double_mesh_nodes(u, i, self.nodes_per_cell)
                self.nb_nodes = self.nodes_per_cell.sum()
        if division:
            self.cell_conc = self.get_cell_concentrations(u, self.conc_operator)
            for i, signal in enumerate(self.signals):
                if signal.transport_mechanism == "Smith model (2006)":
                    u[i], PIN[i] = signal.transport.evolution_conc(
                        u[i], PIN[i], self.D_real, self.S,
                        (self.zone_1.copy(), self.zone_2.copy(), self.zone_3.copy()),
                        self.dt[i], 10000
                    )
        return u, PIN

    def steady_state_concentration(
            self, signal_index, input_time, nodes_per_cell, dX_cell, R
        ):
        """
        Return the steady state concentration of a signal at a given time.

        Only for uniform diffusion.
        """
        i = signal_index
        dX = np.repeat(dX_cell, nodes_per_cell)
        X = dX.cumsum() - dX/2
        diameters_Omega0 = nodes_per_cell * dX_cell
        L_tot = (R * diameters_Omega0).sum()
        x = self.Gamma(X, R, diameters_Omega0)
        transport_i = self.transports[i]
        conc_indices_i = self.conc_indices[i]
        flux_indices_i = self.flux_indices[i]
        D, d = transport_i.D, transport_i.d
        k = np.sqrt(abs(d)/D)
        expkL = np.exp(k*L_tot)
        expminuskL = 1/expkL
        if len(conc_indices_i) == 2:
            # concentration imposed on both boundaries
            u0 = self.BC_values[i][conc_indices_i.index(0)][input_time]
            uL = self.BC_values[i][conc_indices_i.index(-1)][input_time]
            if d == 0:
                # no synthesis, no absorption: affine gradient
                a = (uL - u0) / L_tot
                u_i = u0 + a*x
            elif d > 0:
                # absorption
                A = (u0 - uL*expkL) / (1 - expkL**2)
                B = (uL*expminuskL - u0) / (expminuskL**2 - 1)
                u_i = A*np.exp(k*x) + B*np.exp(-k*x)
            elif d < 0:
                # synthesis
                A = u0
                B = (uL - u0*np.cos(k*L_tot)) / np.sin(k*L_tot)
                u_i = A*np.cos(k*x) + B*np.sin(k*x)
        elif len(conc_indices_i) == 1:
            # a concentration on only one boundary, while a flux is imposed on
            # the other one
            conc_index = conc_indices_i[0]
            if conc_index == 0:
                uB = self.BC_values[i][conc_index][input_time]
                if len(flux_indices_i) == 1:
                    JB = self.BC_values[i][-1][input_time]
                elif len(flux_indices_i) == 0:
                    JB = 0
            elif conc_index == -1:
                uB = self.BC_values[i][-1][input_time]
                if len(flux_indices_i) == 1:
                    JB = self.BC_values[i][0][input_time]
                elif len(flux_indices_i) == 0:
                    JB = 0
            if d == 0:
                # no synthesis, no absorption: affine gradient
                a = -JB/D
                if conc_index == 0:
                    u0 = uB
                elif conc_index == -1:
                    u0 = uB - a*L_tot
                u_i = u0 + a*x
            elif d > 0:
                # absorption
                C = 1/(expkL + expminuskL)
                if conc_index == 0:
                    A = uB*expminuskL - JB/(D*k)
                    B = uB*expkL + JB/(D*k)
                elif conc_index == -1:
                    A = uB - (JB/(D*k))*expminuskL
                    B = uB + (JB/(D*k))*expkL
                u_i = C*(A*np.exp(k*x) + B*np.exp(-k*x))
            elif d < 0:
                # synthesis
                raise NotImplementedError("Flux boundary condition with "
                                          "synthesis is not implemented.")
        return u_i

    def compute_concentration_operator(self, nodes_per_cell):
        """Compute and return the concentration operator."""
        nb_cells = nodes_per_cell.shape[0]
        cum_nodes = nodes_per_cell.cumsum()
        cum_nodes = np.insert(cum_nodes, 0, 0)
        nb_nodes = cum_nodes[-1]
        concentration_operator = np.zeros((nb_cells, nb_nodes))
        for i, nodes in enumerate(nodes_per_cell):
            concentration_operator[i, cum_nodes[i]:cum_nodes[i + 1]] = 1/nodes
        return concentration_operator

    def set_conc_and_flux_masks(self, conc_indices, flux_indices):
        """Compute some useful arrays for boundary concentrations and flux."""
        signals, nodes = [], []
        for signal, conc in enumerate(conc_indices):
            for node in conc:
                signals.append(signal)
                nodes.append(node)

        # conc_indices_for_M
        self.conc_indices_for_M = (signals, nodes, nodes)

        # conc_neighbors_for_M
        self.conc_neighbors_for_M = [[], [], []]
        for signal, node in zip(signals, nodes):
            if node == 0:
                self.conc_neighbors_for_M[0].append(signal)
                self.conc_neighbors_for_M[1].append(0)
                self.conc_neighbors_for_M[2].append(1)
            elif node == -1:
                self.conc_neighbors_for_M[0].append(signal)
                self.conc_neighbors_for_M[1].append(-1)
                self.conc_neighbors_for_M[2].append(-2)
            else:
                self.conc_neighbors_for_M[0].expand([signal, signal])
                self.conc_neighbors_for_M[1].expand([node, node])
                self.conc_neighbors_for_M[2].expand([node - 1, node + 1])

        # flux_cells
        self.flux_cells = list(set(chain.from_iterable(flux_indices)))

    def set_counters_and_masks(self, nodes_per_cell):
        """Create some useful arrays for computing diffusion matrices."""
        nb_nodes = nodes_per_cell.sum()
        self.M_primer = np.zeros((nb_nodes, nb_nodes))
        self.Dlower_counter = nodes_per_cell.copy()
        self.Dlower_counter[0] -= 1
        self.Dupper_counter = nodes_per_cell.copy()
        self.Dupper_counter[-1] -= 1
        self.crossed_mask = nodes_per_cell[:-1].cumsum() - 1
        self.diag_mask = np.diag_indices_from(self.M_primer)
        self.diag_crossed_mask = (self.crossed_mask, self.crossed_mask)
        self.lower_crossed_mask = (self.crossed_mask + 1, self.crossed_mask)

    def init_diffusion_matrix(self, nodes_per_cell):
        """
        Compute an incomplete diffusion matrix based on the structure of the
        cell file. For each signal, the complete matrix will be computed from
        the incomplete matrix, the time steps and the specific coefficients.

        The second element in the output tuple is RdX_flux, the real length of
        the mesh internodes where influx enter.
        """
        RdX = self.R * self.dX_cell
        RdX_flux_diff = RdX[self.flux_cells]
        RdX2 = RdX**2
        RdXcrossed = RdX[:-1] * RdX[1:]
        C = 1 / RdX2
        Cxlower = 2 / (RdX2[1:] + RdXcrossed)
        Cxupper = 2 / (RdX2[:-1] + RdXcrossed)
        Dlower = C.repeat(self.Dlower_counter)
        Dupper = C.repeat(self.Dupper_counter)
        Dlower[self.crossed_mask] = Cxlower
        np.fill_diagonal(self.M_primer[1:, :-1], Dlower)
        Dupper[self.crossed_mask] = Cxupper
        np.fill_diagonal(self.M_primer[:-1, 1:], Dupper)
        # main diagonal
        D = np.zeros(self.nb_nodes)
        D[:-1] -= Dupper
        D[1:] -= Dlower
        np.fill_diagonal(self.M_primer, D)
        return self.M_primer, RdX_flux_diff

    def evolution_matrices(self, nodes_per_cell):
        """Compute and return the evolution matrix of a signal profile."""
        if "uniform diffusion" in self.transport_mechanisms:
            self.set_counters_and_masks(nodes_per_cell)
            M_init, RdX_flux_diff = self.init_diffusion_matrix(nodes_per_cell)
        M = []
        RdX_flux = []
        for i, transport in enumerate(self.transports):
            mechanism = self.transport_mechanisms[i]
            dt = self.dt[i]
            if mechanism == "uniform diffusion":
                RdX_flux_i = RdX_flux_diff
                if transport.steady_state:
                    M_i = None
                else:
                    D, d, P = transport.D, transport.d, transport.P
                    M_i = (D * dt) * M_init
                    M_i[self.diag_mask] += \
                        1 - dt * np.repeat(self.S, nodes_per_cell) - dt*d
                    # active transport through cell walls
                    if P > 0:
                        RdX = self.R * self.dX_cell
                        # diagonal
                        M_i[self.diag_crossed_mask] -= dt * P / RdX[:-1]
                        # lower diagonal
                        M_i[self.lower_crossed_mask] += dt * P / RdX[1:]
            elif mechanism == "simple unidirectional active transport":
                RdX_flux_i = 1/self.D_real[self.flux_cells]
                N = self.nb_cells
                M_i = np.zeros((N, N))
                p, q, d = transport.p, transport.q, transport.d
                p = p * np.ones(N)
                if transport.signals_controling_polar_transport:
                    # Signal concentrations are multiplied together.
                    conc = self.cell_conc[transport.polar_control_indices]
                    p += transport.polar_prefactor * np.prod(conc, axis=0)
                if transport.only_in_cambial_cells:
                    polar_zone = self.dividing_cells.copy()
                    polar_zone_lower = polar_zone.copy()
                    if N-1 in polar_zone:
                        polar_zone_lower = list(polar_zone_lower)
                        polar_zone_lower.remove(N-1)
                        polar_zone_lower = np.array(polar_zone_lower, dtype=int)
                    polar_zone_upper = polar_zone.copy()
                    if 0 in polar_zone:
                        polar_zone_upper = list(polar_zone_upper)
                        polar_zone_upper.remove(0)
                        polar_zone_upper = np.array(polar_zone_upper, dtype=int)
                else:
                    polar_zone = np.arange(0, N)
                    polar_zone_lower = np.arange(0, N-1)
                    polar_zone_upper = np.arange(1, N)
                # diagonal
                D = 1 - 2*q * dt / self.D_real - d * dt - self.S * dt
                D[0] += q * dt / self.D_real[0]    # no diffusion at boundary
                D[-1] += q * dt / self.D_real[-1]  # no diffusion at boundary
                # lower diagonal
                Dlower = q * dt / self.D_real[1:]
                # upper diagonal
                Dupper = q * dt / self.D_real[:-1]
                # additional terms for polar transport
                D[polar_zone] -= p[polar_zone] * dt / self.D_real[polar_zone]
                if transport.carrier_orientation == "toward the xylem":
                    Dlower[polar_zone_lower] += p[polar_zone_lower] * dt \
                                                / self.D_real[polar_zone_lower + 1]
                    if N-1 in polar_zone:  # no polar transport at boundary
                        D[-1] += p[-1] * dt / self.D_real[-1]
                elif transport.carrier_orientation == "toward the cambium":
                    Dupper[polar_zone_upper - 1] += p[polar_zone_upper] * dt \
                                                    / self.D_real[polar_zone_upper - 1]
                    if 0 in polar_zone:  # no polar transport at boundary
                        D[0] += p[0] * dt / self.D_real[0]
                np.fill_diagonal(M_i, D)
                np.fill_diagonal(M_i[1:, :-1], Dlower)
                np.fill_diagonal(M_i[:-1, 1:], Dupper)
            else:
                RdX_flux_i = 1/self.D_real[self.flux_cells]
                M_i = None
            # imposed boundary concentrations
            if M_i is not None:
                for k, signal in enumerate(self.conc_indices_for_M[0]):
                    if signal == i:
                        M_i[self.conc_indices_for_M[1][k],
                            self.conc_indices_for_M[2][k]] = 1.
                for k, signal in enumerate(self.conc_neighbors_for_M[0]):
                    if signal == i:
                        M_i[self.conc_neighbors_for_M[1][k],
                            self.conc_neighbors_for_M[2][k]] = 0.
            M.append(M_i)
            RdX_flux.append(RdX_flux_i)
        return M, RdX_flux

    def grow(self,
             start_time,
             stop_time,
             activation_time=None,
             input_time_step=3600,
             growth_time_step=3600,
             snapshot_time_step=3600,
             nodes_per_cell=3,
             division_identity_signal=None,
             enlargement_identity_signal=None,
             control_signals=None,
             check_conservation=False):
        """Make a radial file grow.

        Parameters
        ----------
        start_time : datetime instance
            Start time of the simulation.
        stop_time : datetime instance
            Stop time of the simulation.
        activation_time : datetime instance, optional, default: 0
            Time at which the cambium is activated (i.e. starts to respond to signals).
        input_time_step : float, optional, default: 3600
            Time step between input value updates, in seconds.
        growth_time_step : float, optional, default: 3600
            Time step for simulating growth, in seconds.
            It is different from the diffusion time step, which is set based on the
            Courant–Friedrichs–Lewy condition and is usually much shorter.
        snapshot_time_step : float, optional, default: 3600
            Time step between snapshots saved, in seconds.
        nodes_per_cell : int, optional
            Initial number of mesh nodes in cells (the default is 3).
        division_identity_signal : Morphogen object
            Signal which sets division identity to cells.
        enlargement_identity_signal : Morphogen object
            Signal which sets enlargement identity to cells.
        contral_signals : list or tuple of Morphogen objects
            Signals which set growth rate.
        check_conservation : bool, optional
            If True, the program will monitor the conservation of the total quantity of
            signals the file (the default is False).

        """
        if activation_time is None:
            activation_delay = start_time.total_seconds()
        else:
            activation_delay = (activation_time - start_time).total_seconds()
        total_time = (stop_time - start_time).total_seconds()
        self.start_time, self.stop_time = start_time, stop_time
        nb_input_samples = int((total_time + snapshot_time_step) / input_time_step) + 2
        self.date_list = self.date_list[:1]
        self.growth_time_step = growth_time_step
        self.conversion = self.data_time_unit / snapshot_time_step
        self.initial_nodes_per_cell = nodes_per_cell
        self.nodes_per_cell = nodes_per_cell*np.ones(self.nb_cells, dtype=int)
        self.nb_nodes = self.nodes_per_cell.sum()
        self.conc_operator = self.compute_concentration_operator(self.nodes_per_cell)
        # Construct the list of signals
        signals = [division_identity_signal] + [enlargement_identity_signal] \
                + list(control_signals)
        # Remove duplicates while preserving order with 'OrderedDict.fromkeys()'
        self.signals = list(OrderedDict.fromkeys(signals))
        self.nb_signals = len(self.signals)
        # Find the indices of the signals setting the division identity, the
        # enlargement identity, and the growth rates (control signals).
        self.div_id_index = self.signals.index(division_identity_signal)
        self.enl_id_index = self.signals.index(enlargement_identity_signal)
        self.control_indices = [i for i, sig in enumerate(self.signals)
                                if sig in control_signals]
        # Set values associated with the signals
        for signal in self.signals:
            signal.set_values(start_time.days,
                              start_time.days + (nb_input_samples - 1) *
                                 input_time_step / self.data_time_unit,
                              nb_input_samples,
                              self.signals)
        # Signal transport mechanisms
        self.transport_mechanisms = [signal.transport_mechanism
                                     for signal in self.signals]
        self.transports = [signal.transport for signal in self.signals]
        # List of time steps dt (one for each signal)
        self.dt = np.zeros(self.nb_signals)
        # length of the reference domaine Omega_0
        self.L0 = self.nb_cells * self.initial_CRD
        # mesh internode in each cell
        self.dX_cell = self.initial_CRD / self.nodes_per_cell
        self.D = self.dX_cell * self.nodes_per_cell
        # initialization of the cell deformation rates S
        self.S = np.zeros(self.nb_cells)
        # time integral of S (\int S*dt)
        self.int_S = np.zeros(self.nb_cells)
        # cumulative cell growth (R)
        self.R = np.exp(self.int_S)
        self.D_real = self.R * self.D
        # empty concentration arrays
        u, PIN = [], []
        for mechanism in self.transport_mechanisms:
            PIN_i = None
            if mechanism == "uniform diffusion":
                u_i = np.zeros(self.nb_nodes)
            elif mechanism == "Smith model (2006)":
                u_i = np.zeros(self.nb_cells)
                PIN_i = np.zeros(self.nb_cells)
            else:
                u_i = np.zeros(self.nb_cells)
            u.append(u_i)
            PIN.append(PIN_i)
        # boundary conditions
        self.BC_values = [np.zeros((2, nb_input_samples))
                          for i in range(self.nb_signals)]
        self.conc_indices = []
        self.flux_indices = []
        flux = None
        conc_index, flux_index = 0, 0
        for i, signal in enumerate(self.signals):
            transport_mechanism = signal.transport_mechanism
            transport = signal.transport
            transport.time = 0  # make sure time is set to 0 for all transports
            conc_indices_i = []
            flux_indices_i = []
            for j, bcond in enumerate(signal.boundary_conditions()):
                node = bcond.get_node()
                values = bcond.values
                if values.shape[0] < nb_input_samples:
                    s = "Not enough values of a boundary condition for the "
                    s += "simulation time!\n"
                    s += str(bcond)
                    raise ValueError(s)
                if bcond.kind == "concentration":
                    conc_indices_i.append(node)
                    conc_index += 1
                elif bcond.kind == "flux":
                    if not bcond.values.any():
                        continue
                    flux_indices_i.append(node)
                    flux = True
                    flux_index += 1
                self.BC_values[i][node] = values
            self.conc_indices.append(conc_indices_i)
            self.flux_indices.append(flux_indices_i)
            # initialize imposed concentrations
            u[i][conc_indices_i] = self.BC_values[i][conc_indices_i][:, 0]
            if transport_mechanism == "uniform diffusion":
                if transport.steady_state:
                    self.dt[i] = input_time_step
                else:
                    # time step given by the Courant–Friedrichs–Lewy condition
                    self.dt[i] = self.dX_cell.min()**2 / (2 * transport.D)
                # set concentration boundary conditions,
                # with linear interpolation within the domain
                if signal.nb_concentrations() == 2:
                    # self.u[i] = self.steady_state_concentration(i, 0)
                    u0 = self.BC_values[i][conc_indices_i.index(0)][0]
                    uL = self.BC_values[i][conc_indices_i.index(-1)][0]
                    u[i] = np.linspace(u0, uL, self.nb_nodes)
            elif transport_mechanism == "simple unidirectional active transport":
                p, q, d = transport.p_max(), transport.q, transport.d
                p = abs(p)
                Dmin = self.D_real.min()
                self.dt[i] = 0.95 * Dmin / (p + 2*q + d*Dmin)
                conc_indices_i = self.conc_indices[i]
                conc = self.BC_values[i][conc_indices_i][:, 0]
                u[i][conc_indices_i] = conc
            elif transport_mechanism == "Smith model (2006)":
                self.dt[i] = 0.005
                u[i], PIN[i] = transport.evolution_conc(
                    u[i], PIN[i], self.D_real, self.S, (None, None, None), self.dt[i],
                    5000
                )
            elif transport_mechanism == "imposed cell concentrations":
                phloem_BC, xylem_BC = None, None
                if 0 in self.conc_indices[i]:
                    phloem_BC = self.BC_values[i][0][0]
                if -1 in self.conc_indices[i]:
                    xylem_BC = self.BC_values[i][1][0]
                u[i] = transport.cell_concentrations(
                        0, self.nb_cells, phloem_BC, xylem_BC)

        self.set_conc_and_flux_masks(self.conc_indices, self.flux_indices)

        time_window = self.growth_time_step
        nb_time_windows = int((total_time + time_window) / time_window)
        nb_time_steps_per_window = np.ceil(time_window / self.dt).astype(int)
        self.dt = time_window / nb_time_steps_per_window

        # turn flux into transferred amounts of matter
        if flux is not None:
            for i, boundaries in enumerate(self.flux_indices):
                self.BC_values[i][boundaries] *= self.dt[i] / self.data_time_unit

        self.cell_conc = self.get_cell_concentrations(u, self.conc_operator)

        # cell identities based on gradient zones
        self.zone_1, self.zone_2, self.zone_3 = self.get_gradient_zones(self.cell_conc)

        if activation_delay > 0:
            inactive = True
        else:
            inactive = False

        # cell identity lists bases on anatomy
        self.inactive_cells, self.growing_cells, self.non_growing_cells, \
                self.cambial_cells, self.dividing_cells \
                = self.determine_cell_identities(inactive=inactive)

        # new data to the Cell objects
        identities = self.get_cells_identities()
        for i, cell in enumerate(self.cells):
            cell.strain_rates.append(self.S[i])
            cell.identities.append(identities[i])

        # records of total signal and total signal evolution to check signal
        # conservation
        self.cum_u_evol = 0.
        self.init_history()
        self.make_history(u, PIN, check_conservation)
        current_time = 0
        self.sample_time = 0

        for tw in range(nb_time_windows):

            input_time = int(current_time / input_time_step)
            M, RdX_flux = self.evolution_matrices(self.nodes_per_cell)

            for i, signal in enumerate(self.signals):
                mechanism = signal.transport_mechanism
                input_time_i = input_time
                signal.transport.time = input_time_i

                # imposed cell concentrations
                if mechanism == "imposed cell concentrations":
                    t_days = input_time_i / self.time_units["day"]
                    phloem_BC, xylem_BC = None, None
                    if 0 in self.conc_indices[i]:
                        phloem_BC = self.BC_values[i][0][input_time_i]
                    if -1 in self.conc_indices[i]:
                        xylem_BC = self.BC_values[i][1][input_time_i]
                    u[i] = signal.transport.cell_concentrations(
                            t_days, self.nb_cells, phloem_BC, xylem_BC)
                    continue

                # uniform diffusion in the steady state approximation
                if mechanism == "uniform diffusion" \
                        and signal.transport.steady_state:
                    u[i] = self.steady_state_concentration(
                        i, input_time,
                        nodes_per_cell=self.nodes_per_cell,
                        dX_cell=self.dX_cell, R=self.R
                    )
                    continue

                # useful values
                current_time_i = current_time
                BC_values_i = self.BC_values[i]
                conc_indices_i = self.conc_indices[i]
                u[i][conc_indices_i] = BC_values_i[conc_indices_i][:, input_time_i]
                with_flux = False
                if self.flux_indices[i]:
                    with_flux = True
                    RdX_flux_i = RdX_flux[i]
                    flux_indices_i = self.flux_indices[i]
                    flux_i = BC_values_i[flux_indices_i][:, input_time_i] / RdX_flux_i

                # Smith model
                if mechanism == "Smith model (2006)":
                    if with_flux:
                        u[i][flux_indices_i] += flux_i
                    u[i], PIN[i] = signal.transport.evolution_conc(
                        u[i], PIN[i], self.D_real, self.S,
                        (self.zone_1.copy(), self.zone_2.copy(), self.zone_3.copy()),
                        self.dt[i], 500
                    )
                    continue

                # transports which can be computed with a matrix product
                u_i = u[i]
                M_i = M[i]
                dt_i = self.dt[i]
                if M_i is None:
                    raise ValueError("Transport ill-defined. A matrix is expected.")
                for t in range(nb_time_steps_per_window[i]):
                    u_i = np.dot(M_i, u_i)
                    # imposed boundary fluxes
                    if with_flux:
                        u_i[flux_indices_i] += flux_i
                    # update boundary conditions if needed
                    current_time_i += dt_i
                    if current_time_i % input_time_step < dt_i:
                        input_time_i += 1
                        u_i[conc_indices_i] = \
                                BC_values_i[conc_indices_i][:, input_time_i]
                        if with_flux:
                            flux_i = BC_values_i[flux_indices_i][:, input_time_i] \
                                     / RdX_flux_i
                u[i] = u_i
                for j, elt in enumerate(u[i]):
                    if elt < 0:
                        print("Negative concentration ({0}) in cell {1}".format(elt, j+1))

            current_time += time_window

            # update cell identities

            if inactive and current_time >= activation_delay:
                inactive = False

            self.inactive_cells, self.growing_cells, self.non_growing_cells, \
                    self.cambial_cells, self.dividing_cells = \
                    self.determine_cell_identities(inactive=inactive)

            # growth
            u, PIN = self.grow_by_one_step_and_check_for_division(u, PIN)

            # reupdate cell identities
            self.inactive_cells, self.growing_cells, self.non_growing_cells, \
                    self.cambial_cells, self.dividing_cells = \
                    self.determine_cell_identities(inactive=inactive)

            # cell identities based on gradient zones
            self.zone_1, self.zone_2, self.zone_3 = \
                    self.get_gradient_zones(self.cell_conc)

            # snapshot
            self.make_history(u, PIN, check_conservation)
            self.date += timedelta(seconds=snapshot_time_step)
            self.date_list.append(self.date)
            identities = self.get_cells_identities()
            for i, cell in enumerate(self.cells):
                cell.indices.append(i)
                cell.dates.append(self.sample_time)
                cell.diameters.append(self.D_real[i])
                cell.strain_rates.append(self.S[i])
                cell.identities.append(identities[i])
            self.sample_time += 1

    def Gamma(self, X, R, D):
        """Map from Omega_0 to Omega_t."""
        x = np.zeros(X.shape)
        B = np.hstack((0, D.cumsum()))
        RD = R*D
        cumGamma = RD.cumsum() - RD
        i = 0
        Bi = B[i]
        Bin = B[i+1]   # Bin stands for Bi next
        for j, Xj in enumerate(X):
            if Xj > Bin:
                i += 1
                Bi, Bin = B[i], B[i+1]
            Delta = Xj - Bi
            x[j] = cumGamma[i] + R[i]*Delta
        return x

    def diameters(self, time=-1):
        """Return cell diameters at a given time."""
        dX = self.dX_cell_history[time]
        R = self.R_history[time]
        if hasattr(self, 'nodes_history'):
            nodes_per_cell = self.nodes_history[time]
        else:
            raise NotImplementedError("'nodes_per_cell' can not be defined.")
        return nodes_per_cell * R * dX

    def length(self, time=-1):
        """Return the total length of the file at a given time."""
        D_real = self.diameters(time)
        return D_real.sum()

    def number_of_cells(self, time=-1):
        """Return tne total number of cells in the file at a given time."""
        return len(self.dX_cell_history[time])

    def cell_accumulation(self):
        """Return the evolution of the number of cells produced."""
        time = len(self.u_history)
        nb_cells = np.empty(time)
        for t in range(time):
            nb_cells[t] = self.number_of_cells(t)
        return nb_cells

    def number_of_cells_in_each_phase(self, time=-1):
        """Return tne number of cells in each phase at a given time."""
        nb_cells = self.number_of_cells(time)
        nb_cambial_cells, nb_enlarging_cells, nb_non_growing_cells = 0, 0, 0
        for cell in self.cells:
            if time in cell.dates:
                i = cell.dates.index(time)
                identity = cell.identities[i]
                if identity == "cambial":
                    nb_cambial_cells += 1
                elif identity == "enlarging":
                    nb_enlarging_cells += 1
                elif identity == "non growing":
                    nb_non_growing_cells += 1
        return (nb_cambial_cells, nb_enlarging_cells, nb_non_growing_cells, nb_cells)

    def total_growth_curve(self):
        """Return the values for plotting the total growth of the cell file.

        Returns
        -------
        length : 1-D ndarray of floats
            Total length of the cell file, in µm.

        """
        time = len(self.u_history)
        length = np.empty(time)
        for t in range(time):
            length[t] = self.length(t)
        return length

    def total_growth_speed_curve(self):
        """Return the values for plotting the growth speed of the cell file.

        Returns
        -------
        speed : 1-D ndarray of floats
            Speed of the total growth of the cell file, in µm.day¯¹.

        """
        length = self.total_growth_curve()
        speed = length[1:] - length[:-1]
        return speed

    def values_for_drawing(self, time):
        """Needed values for drawing the cell file at a given time.

        Returns
        -------
        nodes_per_cell : 1-D ndarray of ints
            Number of mesh nodes per cell.
        x : 1-D ndarray of floats
            Mesh in the real space.
        D_real : 1-D ndarray of floats
            Cell diameters in the real space.
        u : ndarray (number of signals, number of nodes)
            Signal concentrations.

        """
        dX_cell = self.dX_cell_history[time].flatten()
        if hasattr(self, 'nodes_history'):
            nodes_per_cell = self.nodes_history[time]
        else:
            raise NotImplementedError("'nodes_per_cell' can not be defined.")
        dX = np.repeat(dX_cell, nodes_per_cell)
        X = dX.cumsum() - dX/2
        D = nodes_per_cell * dX_cell
        R = self.R_history[time].flatten()
        D_real = D * R
        x = self.Gamma(X, R, D)
        u = self.u_history[time]
        return nodes_per_cell, x, D_real, u

    def plot_conservation(self, representation='absolute', tlim=None):
        """Plot signal conservation."""
        f, ax = plt.subplots()
        if representation == 'absolute':
            ax.plot(self.u_tot_history, 'k+')
            ax.plot(self.cum_u_evol_history, 'r+')
        elif representation == 'ratio':
            u_tot_history = self.u_tot_history.copy()
            u_tot_history[u_tot_history == 0.] = 1.
            cum_u_evol_history = self.cum_u_evol_history.copy()
            cum_u_evol_history[cum_u_evol_history == 0.] = 1.
            ratio = (u_tot_history - cum_u_evol_history)/u_tot_history
            ax.plot(ratio, 'k+')
        else:
            raise ValueError("Enter a valide representation for conservation plot")
        if tlim is not None:
            ax.set_xlim(*tlim)
        plt.show()

    def __str__(self):
        return "Radial cell file {0:d} cells.".format(self.number_of_cells())
